

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Admin System</b>
        </div>
        <strong>Copyright &copy; 2019 <a href="<?php echo base_url(); ?>">Admin System</a>.</strong> All rights reserved.
    </footer>
    
    <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js" type="text/javascript"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-validator.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').datepicker({
                inline: true,
                sideBySide: true,
                dateFormat: 'yy-dd-mm' 
            });

            $('.timepicker').timepicker({
                inline: true,
                sideBySide: true
            });
        });
    </script>
    <script type="text/javascript">
        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('active');
            x.parent().addClass('active');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('active');
            y.parent().addClass('active');
</script>
<script>
  function initMap() {
      var myLatLng = {lat: 22.3038945, lng: 70.80215989999999};
      var input = document.getElementById('locality');
      var autocomplete = new google.maps.places.Autocomplete(input);
      autocomplete.setComponentRestrictions({'country': ['in']});
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var long = place.geometry.location.lng()

        //alert('latitude'+' '+lat+','+ 'longitude'+' '+long);
        document.getElementById('address').value = place.name;
        document.getElementById('cityLat').value = place.geometry.location.lat();
        document.getElementById('cityLng').value = place.geometry.location.lng();

      });
      var map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 13
      });
    
      var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!',
            draggable: true
          });
    
       google.maps.event.addListener(marker, 'dragend', function(marker) {
          var latLng = marker.latLng;
          document.getElementById('cityLat').value = latLng.lat();
          document.getElementById('cityLng').value = latLng.lng();
          // alert(latLng.lat());
       });
  }

</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDqP1TdIK9QoKX6Ym5DVgrtfTV7PNxMGKw&callback=initMap"></script>
  </body>
</html>