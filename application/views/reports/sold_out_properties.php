<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Reports Management
        <!-- <small>Sold Out Properties</small> -->
      </h1>
    </section>
   
    <section class="content">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Brokers & Property</a></li>
    <li class="breadcrumb-item active" aria-current="page">Sold Out Properties</li>
  </ol>
</nav>

      

        <?php if ($this->session->flashdata('msg')): ?>
            <div class="alert alert-success">
                <strong>Success!</strong> Property Status has been changed successfully.
            </div>
        <?php endif ?>

        <?php //$this->load->view('flash-data'); ?>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">`
                <div class="box-header">
                    <h3 class="box-title">Sold Out Properties Lists</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>CommercialRentList" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                    

                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>Property Type</th>
                            <th>Price</th>
                            <th>Location</th>
                            <th>Reported By</th>
                            <th>Marked As Sold Out</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        <?php
                        if(!empty($soldOutProperties))
                        {
                            foreach($soldOutProperties as $record)
                            {
                        ?>
                        <tr>
                            <td><?php echo $record->property_type; ?></td>
                            <td><?php echo $record->price; ?></td>
                            <td><?php echo $record->location; ?></td>
                            <td><a href="<?= base_url('user/userDetail/'.$record->userid); ?>"><?php echo $record->username; ?></a></td>
                            <td><?php echo ($record->is_sold_out==1)?"Yes":"No"; ?></td>
                            <td class="text-center">
                                 <?php
                              $permision = unserialize($_SESSION['permision']);
                              echo ' <a class="btn btn-sm btn-info" href="'.base_url('property/markAsSoldOut/'.$record->propertyid).'" title="Mark As Sold-out" ><i class="fa fa-paper-plane-o"></i></a>';
                            if(isset($permision['reporttabs']['broker_delete'])=="on")
                             {
                                echo '<a class="btn btn-sm btn-info" href="'.WEB_URL.'property/detail/'.$record->propertyid.'/?property=sale'.'" target="_blank" data-url="deleteCommercialRentProperty" data-msg="Commercial Rent Property" title="Delete"><i class="fa fa-info" ></i></a>';
                             }
                           
                             
                             if($role == ROLE_ADMIN)
                           {

                            ?>
                          <a class="btn btn-sm btn-info" href="<?php echo base_url('property/markAsSoldOut/'.$record->propertyid); ?>" title="Mark As Sold-out" ><i class="fa fa-paper-plane-o"></i></a>

                                <a class="btn btn-sm btn-info" href="<?php echo WEB_URL.'property/detail/'.$record->propertyid.'/?property=sale'; ?>" target="_blank" data-url="deleteCommercialRentProperty" data-msg="Commercial Rent Property" title="Delete"><i class="fa fa-info" ></i></a>

                                <?php
                            }
                                ?>  


                                

                                <!-- <a class="btn btn-sm btn-danger deleteProperty" href="#" data-propertyid="<?php echo $record->propertyid; ?>" data-url="deleteCommercialRentProperty" data-msg="Commercial Rent Property" title="Delete"><i class="fa fa-trash"></i></a> -->
                            </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "editResidentialResaleProperty/" + value);
            jQuery("#searchList").submit();
        });
            jQuery(".report-management").addClass("menu-open");
       jQuery(".report-management-view").css("display","block");

       jQuery(".brokers").addClass("menu-open");
       jQuery(".brokers-view").css("display","block");
    });
</script>
