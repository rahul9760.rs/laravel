    <?php
// echo "<pre>"; print_r($_SESSION); die;
$permision=unserialize($_SESSION['permision']);

?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-star"></i> Reviews & ratings
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
       <!--  <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('reviews/addNewReview'); ?>"><i class="fa fa-plus"></i> Add Review</a>
                </div>
            </div>
        </div> -->
        
        <div class="row">
            <?php
            if($role == ROLE_ADMIN)
            {
            echo '<div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="'.base_url('reviews/addNewReview').'"><i class="fa fa-plus"></i>Add Review</a>
                </div>
            </div>
        </div>';
         }

        ?>
                         <?php

                             if(isset($permision['ratingtabs']['rating_add'])=="on")
                             {
                            ?>
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('plans/addNewPlan'); ?>"><i class="fa fa-plus"></i>Add Review</a>
                </div>
            </div>
        </div>
        <?php
            }

        ?>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Rating & Reviews</a></li>
          <li class="breadcrumb-item active" aria-current="page">List</li>
           <!-- <li class="breadcrumb-item active" aria-current="page">Rent</li> -->
        </ol>
      </nav>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Review List</h3>
                    <div class="box-tools">
                        <form action="" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                        <th>Review</th>
                        <th>Review By</th>
                        <th>Rating in start</th> 
                        <th>Created On</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                    <tr>
                        <td><?php echo $record->review; ?></td>
                        <td><?php echo $record->review_by; ?></td>
                        <td><?php echo $record->rating; ?></td> 
                        <td><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td>
                        <td class="text-center"> 
                               <?php
                              $permision = unserialize($_SESSION['permision']);
                             if(isset($permision['ratingtabs']['rating_edit'])=="on")
                             {
                                echo '<a class="btn btn-sm btn-info" href="'.base_url().'reviews/editOld/'.$record->id.'" title="Edit"><i class="fa fa-pencil"></i></a>';
                             } if(isset($permision['ratingtabs']['rating_delete'])=="on")
                             {
                                echo '<a class="btn btn-sm btn-danger deleteReview" href="#" data-planid="'.$record->id.'" title="Delete"><i class="fa fa-trash"></i></a>';
                             }
                             
                             if($role == ROLE_ADMIN)
                           {

                            ?>

                             <a class="btn btn-sm btn-info" href="<?php echo base_url().'reviews/editOld/'.$record->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deleteReview" href="#" data-planid="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                                <?php
                            }
                                ?>
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php //echo //$this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "plans/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
