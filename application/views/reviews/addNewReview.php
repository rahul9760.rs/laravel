<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-star"></i> Reviews & ratings
            <!-- <small>Add / Edit User</small> -->
        </h1>
    </section>

    <section class="content">
 <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>reviews">Reviews & ratings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add New</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Review</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <?php if($this->session->flashdata('message')){?>
                    <div class="alert alert-<?php echo $this->session->flashdata('type');?>">
                       <?php echo $this->session->flashdata('message');?>
                    </div>
                    <?php } ?>
                    <form role="form" id="addPlan" action="" method="post" role="form">                       
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Review</label>
                                        <textarea class="form-control required" value="" id="" name="review"></textarea>
                                        <!-- <input type="text"  maxlength="128"> -->
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">review by</label>
                                        <input type="text" class="form-control required" id="" value="" name="review_by" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Rating</label> 
                                        <select class="form-control" name="rating" id="">
                                            <option value="">Rating</option>
                                            <option value="1">1 Star</option>
                                            <option value="2">2 Star</option>
                                            <option value="3"> 3 Star</option>
                                            <option value="4">4 Star</option>
                                            <option value="5">5 Star</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Profile image</label>
                                        <input type="text" class="form-control required" id="" value="" name="designation" maxlength="128">
                                    </div>
                                </div>
                                <div class="add_one_more">
                                    
                                </div>
                            </div>
                            
                             
                            <!-- usuuuus -->

                        </div><!-- /.box-body -->
                        <!-- <input type="button" class="btn btn-primary add-more" value="Add New Specification" />  -->
                        
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    var i=2;
    $("body").on("click",".add-more",function(){ 
        var html = $(".specification").first().clone();
     
          $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-danger remove'>- Remove</a>");
      
        var htm=`<div class="col-md-12 specification" id="spc_`+i+`">
                                    <div class="form-group">
                                        <label for="cpassword">Specification `+i+`</label>
                                        <input type="text" class="form-control required equalTo" id="specification_`+i+`" name="plan_specification[]" maxlength="20">
                                        <span class="remove_sp" id="remove_sp_`+i+`" style="color:red">X</span>
                                    </div>
                                </div>`; 
          $(".add_one_more").append(htm);
      
//     serialize($this->input->post('name'))
       i++;
    });

    $("body").on("click",".remove_sp",function(){ 
        var id=$(this).attr('id');
        var ar=id.split('_');
        var rid=ar[2];
        $('#spc_'+rid).remove();
    });
});


</script>