<?php
$planId = $reviewInfo->id;
$review = $reviewInfo->review;
$review_by = $reviewInfo->review_by;
$designation = $reviewInfo->designation;
$rating = $reviewInfo->rating;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-star"></i> Reviews & ratings
        <!-- <small>Add / Edit</small> -->
      </h1>
    </section>
    
    <section class="content">
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>reviews">Reviews & ratings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Reviews</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Ratings & Reviews</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>reviews/editReview" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Review</label>
                                        <input type="text" class="form-control" id="" placeholder="Plan Name" name="review" value="<?php echo $review; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $planId; ?>" name="planId" id="planId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">Review By</label>
                                        <input type="text" class="form-control" id="" placeholder="Enter Plan Price" name="review_by" value="<?php echo $review_by; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="validity">Rating</label>
                                        <select class="form-control" name="rating" id="">
                                            <option value=""  >Select</option>
                                            <option value="1" <?php if($rating==1){ echo "selected";}  ?>>1 Star</option>
                                            <option value="2" <?php if($rating==2){ echo "selected";}  ?>>2 Star</option>
                                            <option value="3" <?php if($rating==3){ echo "selected";}  ?>>3 Star</option>
                                            <option value="4" <?php if($rating==4){ echo "selected";}  ?>>4 Star</option>
                                            <option value="5" <?php if($rating==5){ echo "selected";}  ?>>5 Star</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Designation</label>
                                        <input type="text" class="form-control" id="" placeholder="Plan Name" name="designation" value="<?php echo $designation; ?>" maxlength="128">
                                    </div> 
                                </div> 
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                           <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="plan_id">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>