  <?php
// echo "<pre>"; print_r($_SESSION); die;
$permision=unserialize($_SESSION['permision']);

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <style type="text/css">
        .payment{
            color: green;
            font-family: monospace;
        }
    </style>
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Service Enquiries
        <!-- <small></small> -->
      </h1>
    </section>
    <section class="content">
       <!--  <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('payments/addNewPayment'); ?>"><i class="fa fa-plus"></i> External Customer</a>
                </div>
            </div>
        </div> -->
         <div class="row">
            <?php if($role == ROLE_ADMIN) {
            echo '<div class="row">
                <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="'.base_url('payments/addNewPayment').'"><i class="fa fa-plus"></i>External Customer</a>
                </div>
                </div>
            </div>';
            } ?>
        <?php  if($permision['ratingtabs']['rating_add']=="on") {?>
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('plans/addNewPlan'); ?>"><i class="fa fa-plus"></i>External Customer</a>
                </div>
            </div>
        </div>
        <?php } ?>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Service Enquiries</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo $enType; ?></li>
           <li class="breadcrumb-item active" aria-current="page">View</li>
        </ol>
      </nav>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                
                <div class="box-header">
                    <h3 class="box-title">External Customer List</h3>
                    <div class="box-tools">
                        <form action="" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th> 
                        <th>Subject</th> 
                        <th>Created On</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                            if($record->status=='success'){ $color= "style=color:green;"; }else{ $color ="style=color:red;" ;}
                    ?>
                    <tr>
                        <td><?php echo $record->name ?></td>
                        <td><?php echo $record->mobile ?></td>
                        <td><?php echo $record->email ?></td> 
                        <td><?php echo "N/A" ?></td> 
                        <td><?php echo $record->created_at; ?></td>
                        <td class="text-center"> 
                         <?php
                              $permision = unserialize($_SESSION['permision']);
                             if( $permision['externaltabs']['external_delete']=="on")
                             {
                                echo '<a class="btn btn-sm btn-danger deletePayment" href="#" data-paymentid="'.$record->id.'" title="Delete"><i class="fa fa-trash"></i></a>';
                             } 
                             
                             if($role == ROLE_ADMIN)
                           {

                            ?>
                            <a class="btn btn-sm btn-danger deletePayment" href="#" data-paymentid="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                                <?php
                            }
                                ?>  
                           
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "customerList/" + value);
            jQuery("#searchList").submit();


       // jQuery(".residential-menu").addClass("menu-open");
       // jQuery(".residential-view").css("display","block");
        });
        var url_string = window.location; //window.location.href
        var url = new URL(url_string);
        var c = url.searchParams.get("en");
        if(c==='why_myzerobroker')
        {
           jQuery("a[href='http://admin.laravelschool.in/user/customers?en=whymzb']").addClass("active");
        jQuery("a[href='http://admin.laravelschool.in/user/customers?en=whymzb']").css("color","#fff");
        }else{
             jQuery("a[href='http://admin.laravelschool.in/user/customers?en=etc']").addClass("active");
        jQuery("a[href='http://admin.laravelschool.in/user/customers?en=etc']").css("color","#fff");
        }

        jQuery(".enquirymanagement").addClass("menu-open");
       jQuery(".reportmanagement-view").css("display","block");
    });
</script>
