<div class="content-wrapper">
<?php
      $this->load->helper('form');
      $error = $this->session->flashdata('error');
      if($error)
      {
  ?>
  <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo $this->session->flashdata('error'); ?>                    
  </div>
  <?php } ?>
  <?php  
      $success = $this->session->flashdata('success');
      if($success)
      {
  ?>
  <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo $this->session->flashdata('success'); ?>
  </div>
  <?php } ?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <i class="fa fa-home" aria-hidden="true"></i> Property Management
         <!-- <small>Add / Edit Property</small> -->
      </h1>
   </section>
   <section class="content">
      <form data-toggle="validator" role="form" id="ResidentialResaleAddProperty" action="<?php echo base_url() ?>ResidentialResaleAddProperty" method="post">
         <div class="row">
            <!-- left column -->
            <div  class="col-sm-12">
               <!-- <h3>Left Tabs</h3> -->
                <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Property</a></li>
    <li class="breadcrumb-item active" aria-current="page">Residential</li>
     <li class="breadcrumb-item active" aria-current="page">Resale</li>
     <li class="breadcrumb-item active" aria-current="page">Add New</li>
  </ol>
</nav>
               <hr/>
               <div class="col-xs-3">
                  <!-- required for floating -->
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs tabs-left sideways">
                     <li class="active"><a href="#Property-Details" data-toggle="tab">Property Details</a></li>
                     <li><a href="#Locality-Details" data-toggle="tab">Locality Details</a></li>
                     <li><a href="#Resale-Details" data-toggle="tab">Resale Details</a></li>
                     <li><a href="#Gallery" data-toggle="tab">Gallery</a></li>
                     <li><a href="#Amenities" data-toggle="tab">Amenities</a></li>
                     <li><a href="#Schedule" data-toggle="tab">Schedule</a></li>
                     <li><a href="#information" data-toggle="tab">Information</a></li>
                  </ul>
               </div>
               <div class="col-xs-9">
                  <!-- Tab panes -->
                  <div class="tab-content">
                     <div class="tab-pane active" id="Property-Details">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="apartment_type">Apartment Type *</label>
                                 <select class="form-control required" id="apartment_type" name="Property[apartment_type]" data-error="Please enter name field." required>
                                    <option>Select</option>
                                    <?php 
                                       $apartmenttypelist = array(
                                          'Apartment'=>'Apartment',
                                          'Independent House/Villa'=>'Independent House/Villa',
                                          'gated community villa'=>'Gated Community Villa',
                                          'Standalone Building'=>'Standalone Building'
                                       );
                                       if(!empty($apartmenttypelist)){
                                         foreach ($apartmenttypelist as $key => $value){
                                             ?>
                                         <option value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                             <?php
                                         }
                                       }
                                    ?>
                                 </select>
                                 <div class="help-block with-errors"></div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="apartment_name">Apartment Name *</label>
                                 <input type="text" class="form-control" id="apartment_name" name="Property[apartment_name]" required>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="address">BHK Type *</label>
                                 <select class="form-control required" id="bhk_type" name="Property[bhk_type]" required>
                                    <option>Select</option>
                                    <option value="RK1">1 RK</option>
                                    <option value="1">1 BHK</option>
                                    <option value="2">2 BHK</option>
                                    <option value="3">3 BHK</option>
                                    <option value="4">4 BHK</option>
                                    <option value="4+">4+ BHK</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="Ownership_Type">Ownership Type *</label>
                                 <select class="form-control required" id="Ownership_Type" name="Property[ownership_type]" required>
                                    <option>Select</option>
                                   <option value="Lease">On Lease</option>
                                   <option value="Owned">Self Owned</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="floor">Floor *</label>
                                 <select class="form-control required" id="floor" name="Property[floor]" required>
                                    <option>Select</option>
                                 <?php
                                  
                                    if(!empty($floor)){
                                      foreach ($floor as $value){ ?>
                                      <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                     <?php }
                                    }
                                 ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="total_floor">Total Floor *</label>
                                 <select class="form-control required" id="total_floor" name="Property[total_floor]" required >
                                    <option>Select</option>
                              <?php
                                 if(!empty($top_floor)){
                                   foreach ($top_floor as $value){ ?>
                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                  <?php }
                                 }
                              ?>
                                 </select>
                              </div>
                           </div>
                        <!-- </div>
                        <div class="row"> -->
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="Floar_Type">Floar Type *</label>
                                 <select class="form-control required" id="Floar_Type" name="Property[floor_type]" required>
                                    <option value="">Select</option>
                                    <option value="Vitrified Tiles">Vitrified Tiles</option>
                                    <option value="Mosaic">Mosaic</option>
                                    <option value="Marble/Granite">Marble/Granite</option>
                                    <option value="Wooden">Wooden</option>
                                    <option value="Cement">Cement</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="property_age">Property Age *</label>
                                 <select class="form-control required" id="property_age" name="Property[property_age]" required>
                                    <option>Select</option>
                                    <option value="Under Construction">Under Construction</option>
                                    <option value="Less than one year">Less than one year</option>
                                    <option value="1-3 Years">1-3 Years</option>
                                    <option value="3-5 Years">3-5 Years</option>
                                    <option value="5-10">5-10 Years</option>
                                    <option value="More than 10 Years">More than 10 Years</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="facing">Facing *</label>
                                 <select class="form-control required" id="facing" name="Property[facing]" required>
                                 <option>Select</option> 
                                 <option value="North">North</option>
                                 <option value="South">South</option>
                                 <option value="East">East</option>
                                 <option value="West">West</option>
                                 <option value="North-East">North-East</option>
                                 <option value="SE">South-East</option>
                                 <option value="North-West">North-West</option>
                                 <option value="South-West">South-West</option>
                                 <option value="Don't Know">Don't Know</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group" >
                                 <label for="property_size">Property Size *</label>
                                 <input type="text" class="form-control" id="property_size" name="Property[area_sq_ft]" required>
                                 <div class="prpty_append">Sq ft</div>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group" >
                                 <label for="No_of_Units">No of Units *</label>
                                 <input type="text" class="form-control" id="No_of_Units" name="Property[no_of_units]" required>
                                 <div class="prpty_append">Sq ft</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Locality  -->
                     <div class="tab-pane" id="Locality-Details">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="city">City *</label>
                                 <input type="text" class="form-control" id="city" name="Locality[city]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="locality">Locality *</label>
                                 <input type="text" class="form-control" id="locality" name="Locality[locality]" required>
                                 <input type="hidden" id="address" name="city2" />
                                 <input type="hidden" id="cityLat" name="Locality[locality_lat]" />
                                 <input type="hidden" id="cityLng" name="Locality[locality_long]" />
                                 <input type="hidden" name="sale" value="sale" /> 
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="street_addres">Street Addres *</label>
                                 <input type="text" class="form-control" id="street_addres" name="Locality[street_addres]" required>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Resale-Details -->
                     <div class="tab-pane" id="Resale-Details">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="no_of_lease_years">No Of Lease Years? *</label>
                                 <input type="text" class="form-control" id="no_of_lease_years" name="Resale[no_of_lease_years]" required>
                              </div>
                           </div>
                       <!--  </div>
                        <div class="row"> -->
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="expected_cost">Expected Cost *</label>
                                 <input type="text" class="form-control" id="expected_cost" name="Resale[expected_rent]" required>
                              </div>
                           </div>
                           <!-- <div class="col-md-6">
                              <div class="form-group">
                                 <label for="is_currently_under_loan">Is Currently Under Loan *</label>
                                 <input type="text" class="form-control" id="is_currently_under_loan" name="Resale[is_currently_under_loan]" required>
                              </div>
                           </div> -->
                           <div class="col-md-6">
                              <div class="form-group">
                                <label class="custom-control-label" for="is_currently_under_loan">Is Currently Under Loan *</label>
                                 <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="is_currently_under_loan" value="Yes" name="Resale[is_currently_under_loan]" required>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                <label class="custom-control-label" for="is_negotiable">Is Negotiable *</label>
                                 <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="price_negotiable" value="Yes" name="Resale[price_negotiable]">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="maintenance_cost">Maintenance Cost *</label>
                                 <input type="text" class="form-control" id="maintenance_cost" name="Resale[maintenance_cost]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="available_forms">Availablle From *</label>
                                 <input type="text" class="form-control datetimepicker" id="availabble_from" name="Resale[available_from]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="kitchen_type">Kitchen Type *</label>
                                 <select class="form-control required" id="kitchen_type" name="Resale[kitchen_type]" required>
                                    <option value="">Select</option>
                                    <option value="Modular">Modular</option>
                                    <option value="Covered Shelves">Covered Shelves</option>
                                    <option value="Open Shelves">Open Shelves</option>
                                 </select>
                              </div>
                           </div>                       
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="furnishing">Furnishing *</label>
                                 <select class="form-control required" id="furnishing" name="Resale[furnishing]" required>
                                    <option value="">Select</option>
                                    <option value="Fully furnished">Fully furnished</option>
                                    <option value="Semi-furnished">Semi-furnished</option>
                                    <option value="Unfurnished">Unfurnished</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="parking">Parking *</label>
                                 <select class="form-control" id="parking" name="Resale[parking]" required>
                                    <option value="">Select</option>
                                    <option value="Bike">Bike</option>
                                    <option value="Car">Car</option>
                                    <option value="Bike and Car">Bike and Car</option>
                                    <option value="None">None</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="description">Description *</label>
                                 <input type="text" class="form-control" id="description" name="Resale[description]" required>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Gallery -->
                     <div class="tab-pane" id="Gallery">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <span class="input-group-text" id="upload_images01">Upload</span>
                                 </div>
                                 <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="upload_images"
                                       aria-describedby="upload_images01" name="Gallery[upload_images]">
                                    <label class="custom-file-label" for="upload_images">Choose file</label>
                                 </div>
                              </div>
                           </div>
                           <!-- <div class="col-md-6">
                              <div class="form-group">
                                  <label for="dob">Apartment Name *</label>
                                  <input type="text" class="form-control" id="dob" name="dob" >
                              </div>
                              </div> -->
                        </div>
                     </div>
                     <!-- Amenities -->
                     <div class="tab-pane" id="Amenities">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="bathrooms">Bathrooms *</label>
                                 <input type="text" class="form-control" id="bathrooms" name="Amenities[bathrooms]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="water_supply">Water Supply *</label>
                                 <select class="form-control required" id="water_supply" name="Amenities[water_supply]" required>
                                    <option value="">Select</option>
                                    <option value="Corporation">Corporation</option>
                                    <option value="Borewell">Borewell</option>
                                    <option value="Both">Both</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="gym">Gym *</label>
                                 <select class="form-control required" id="gym" name="Amenities[gym]" required>
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="balcony">Balcony *</label>
                                 <input type="text" class="form-control" id="balcony" name="Amenities[balcony]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="power_backup">Power Backup *</label>
                                 <select class="form-control required" id="power_backup" name="Amenities[power_backup]" required>
                                    <option value="">Select</option>
                                    <option value="full">Full</option>
                                    <option value="Partial">Partial</option>
                                    <option value="None">None</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="gated_security">Gated Security *</label>
                                 <select class="form-control" id="gated_security" name="Amenities[gated_security]" required>
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="who_will_show_house">Who Will Show The House *</label>
                                 <select class="form-control" id="who_will_show_house" name="Amenities[who_will_show_house]" required>
                                    <option value="">Select</option>
                                    <option value="I will show">I will show</option>
                                    <option value="Need Help">Need Help</option>
                                    <option value="Neighbours">Neighbours</option>
                                    <option value="Others">Others</option>
                                    <option value="Security">Security</option>
                                    <option value="Tenants">Tenants</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="secondary_number">Secondary Number *</label>
                                 <input type="text" class="form-control" id="secondary_number" name="Amenities[secondary_number]" required>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <label for="select_the_amenities_available">Select The Amenities Available *</label>
                              <div class="row">
                                 <?php 
                              $checkarr= array(
                               'Lift'=>array('Lift','fa-square'),
                                'Internet Services' => array('Internet Services','fa-internet-explorer'),
                                'Air Conditioner' => array('Air Conditioner','fa-window-maximize'),
                                'Club House' => array('Club House','fa-cc-diners-club'),
                                'Intercom' =>array('Intercom','fa-american-sign-language-interpreting'),
                                'Swimming Pool' => array('Swimming Pool','fa-bath'),
                                "Children's Play Area" => array("Children's Play Area",'fa-futbol-o'),
                                'Fire Safety' => array('Fire Safety','fa-fire-extinguisher'),
                                'Servant Room' => array('Servant Room','fa-child'),
                                'Shopping Center' => array('Shopping Center','fa-shopping-cart'),
                                'Gas Pipeline' => array('Gas Pipeline','fa-sun-o'),
                                'Park' => array('Park','fa-tree'),
                                'Rain Water Harvesting' => array('Rain Water Harvesting','fa-cloud'),
                                'Sewage Treatment Plant' => array('Sewage Treatment Plant','fa-medkit'),
                                'House Keeping' => array('House Keeping','fa-female'),
                                'Power Backup' => array('Power Backup','fa-battery-full'),
                                'Visitor Parking' => array('Visitor Parking','fa-product-hunt')
                                  );
                               foreach($checkarr as $key=>$check){
                                ?>
                                  <div class="col-md-6 col-sm-6">
                                     <div class="formCheckbox">
                                        <input type="checkbox" name="amenitiesarr[]" value="<?php echo $key; ?>" id="<?php echo $key; ?>">
                                        <i class="fa <?php echo $check[1]; ?>" aria-hidden="true"></i>
                                        <label for="<?php echo $key; ?>"><?php echo $check[0]; ?></label>
                                        <span class="amenities lift"></span>
                                     </div>
                                  </div>
                               <?php }
                               ?>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Schedule -->
                     <div class="tab-pane" id="Schedule">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="availability">Availability *</label>
                                 <select class="form-control" id="availability" name="Schedule[availability]" required>
                                    <option value="">Select</option>
                                    <option value="EVERYDAY">Everyday (Monday - Sunday)</option>
                                    <option value="WEEKDAY">Weekdays (Monday - Friday)</option>
                                    <option value="WEEKEND">Weekends (Saturday - Sunday)</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="start_time">Start Time *</label>
                                 <input type="text" class="form-control timepicker" id="start_time" name="Schedule[start_time]" required>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="end_time">End Time *</label>
                                 <input type="text" class="form-control timepicker" id="end_time" name="Schedule[end_time]" required>
                              </div>
                           </div>
                        <!--    <div class="col-md-4">
                              <div class="form-group">
                                 <labe -->
                                 <!-- <input type="text" class="form-control" id="available_all_day" name="Schedule[available_all_day]" required> -->
                                <!--  <div class="custom-control custom-checkbox">
                                     <input type="checkbox" value="true" id="available_all_day" name="Schedule[available_all_day]" required>
                                 </div>
                              </div> -->
                           </div>
                        </div>
                     </div>
                     <!-- Information -->
                  <div class="tab-pane" id="information">
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="do_you_have_sale_deed_certificate">Do You Have Sale Deed Certificate *</label>
                              <select  class="form-control required" id="do_you_have_sale_deed_certificate" name="Information[sale_deed_certificate]" required>
                                 <option datat-value="">Select</option>
                                 <option value="Yes">Yes</option>
                                 <option value="No">No</option>
                                 <option value="Don't Know">Don't Know</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="select_have_you_paid_propery_tax">Select Have You Paid Propery Tax *</label>
                              <select  class="form-control required" id="select_have_you_paid_propery_tax" name="Information[paid_propery_tax]" required>
                                 <option datat-value="">Select</option>
                                 <option value="Yes">Yes</option>
                                 <option value="No">No</option>
                                 <option value="Don't Know">Don't Know</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <label for="do_you_have_occupancy_certificate">Do You Have Occupancy Certificate *</label>
                              <select  class="form-control required" id="do_you_have_occupancy_certificate" name="Information[occupancy_certificate]" required>
                                 <option datat-value="">Select</option>
                                 <option value="Yes">Yes</option>
                                 <option value="No">No</option>
                                 <option value="Don't Know">Don't Know</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>
                  </div>
               </div>
               <!--  <div class="clearfix"></div> -->
            </div>
         </div>
         <div class="box-footer">
            <input type="submit" class="btn btn-primary" value="Submit" />
            <input type="reset" class="btn btn-default" value="Reset" />
         </div>
      </form>
   </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("a[href='http://localhost/laravelschool/ResidentialResaleList']").addClass("active");
        jQuery("a[href='http://localhost/laravelschool/ResidentialResaleList']").css("color","#fff");
         jQuery(".property").addClass("menu-open");
       jQuery(".residential").css("display","block");
       jQuery(".residential-menu").addClass("menu-open");
       jQuery(".residential-view").css("display","block");
    });
</script>