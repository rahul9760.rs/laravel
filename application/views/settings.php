 <?php
// echo "<pre>"; print_r($_SESSION); die;
$permision=unserialize($_SESSION['permision']);
// print_r($permision);
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-gear"></i> Settings 
        <!-- <small>Edit Settings</small> -->
      </h1>
    </section>
    
    <section class="content">
    <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <!-- <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>plans">Plan</a></li> -->
    <li class="breadcrumb-item active" aria-current="page">System Settings</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                <div class="box box-primary">
                    
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addUser" action="" method="post" role="form" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Header Logo</label>
                                        <input type="file" class="form-control required" value="" id="toplogo" name="toplogo">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Footer Logo</label>
                                        <input type="file" class="form-control required" id="footerlogo" value="" name="footerlogo">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control required" id="password" name="password" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="20">
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required digits" id="mobile" value="<?php echo set_value('mobile'); ?>" name="mobile" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control required" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->id ?>" <?php if($rl->id == set_value('role')) {echo "selected=selected";} ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>    
                            </div> -->
                            <!-- jdj -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Footer Text</label>
                                        <textarea  class="form-control" rows="5" id="footer_text" name="post[footer_text]"><?php echo isset($result->footer_text)?$result->footer_text:""; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">Phone</label>
                                        <input type="text" class="form-control" id="phone" name="post[phone]" value="<?php echo isset($result->phone)?$result->phone:""; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Email</label>
                                        <input type="text" class="form-control" id="email" name="post[email]" value="<?php echo isset($result->email)?$result->email:""; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">YouTube Vedio URL</label>
                                        <input type="text" class="form-control" id="vedio" name="post[vedio]" value="<?php echo isset($result->vedio)?$result->vedio:""; ?>">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">city</label>
                                        <input type="text" class="form-control" id="city" name="city">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value="1" name="get_whats_app_updates">get updates on whatsapp  ?</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- usuuuus -->

                        </div><!-- /.box-body -->
                                 <?php

                             if($permision['settingtabs']['setting_edit']=="on")
                             {
                            ?>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Save Changes" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                        <?php
                    }
                     if($role == ROLE_ADMIN)
                           {
                             ?>

                          
                            <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Save Changes" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                                <?php
                            }
                                ?>  
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "editResidentialFlatmateProperty/" + value);
            jQuery("#searchList").submit();
        });
         jQuery(".settings").addClass("menu-open");
       jQuery(".settings").css("display","block");
       jQuery(".settings-menu").addClass("menu-open");
       jQuery(".settings-view").css("display","block");
    });
</script>