<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
      <!--   <small>Add / Edit User</small> -->
      </h1>
    </section>
   

    <section class="content">
     <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>userListing">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add User</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <form role="form" id="addUser" action="<?php echo base_url() ?>addNewUser" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control required" value="<?php echo set_value('fname'); ?>" id="fname" name="fname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="text" class="form-control required email" id="email" value="<?php echo set_value('email'); ?>" name="email" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control required" id="password" name="password" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control required equalTo" id="cpassword" name="cpassword" maxlength="20">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control required digits" id="mobile" value="<?php echo set_value('mobile'); ?>" name="mobile" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control required" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->id ?>" <?php if($rl->id == set_value('role')) {echo "selected=selected";} ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>    
                            </div>
                            <!-- jdj -->
                       <!--      <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea  class="form-control" rows="5" id="address" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">Datge of birth</label>
                                        <input type="text" class="form-control" id="dob" name="dob" >
                                    </div>
                                </div>
                            </div>
 -->                          <!--   <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control" id="country" name="country">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">state</label>
                                        <input type="text" class="form-control" id="state" name="state" maxlength="20">
                                    </div>
                                </div>
                            </div> -->
                         <!--    <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">city</label>
                                        <input type="text" class="form-control" id="city" name="city">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value="1" name="get_whats_app_updates">get updates on whatsapp  ?</label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                               <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Permissions</li>
  </ol>
</nav>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="user_permision"  name="permissions[usertabs]"> User Tab</label>
                                    </div>
                                    <div class="user_permision" style="display: none;">
                                  <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" name="permissions[usertabs][payment_history]"> Payment History</label></li>
                                   
                                   
                                       <li><label><input type="checkbox" name="permissions[usertabs][login_history]"> Login History</label></li>
                                   <li><label><input type="checkbox"  name="permissions[usertabs][mail]"> Mail</label></li>
                                      <li> <label><input type="checkbox"  name="permissions[usertabs][user_edit]"> Edit</label></li>
                                   
                                  <li>
                                       <label><input type="checkbox"  name="permissions[usertabs][user_delete]"> Delete</label></li>
                                  </ul> 
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="plan_permision"  name="permissions[plantabs]"> Plans Tab</label>
                                    </div>
                                    <div class="plan_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                     <li> <label><input type="checkbox"  name="permissions[plantabs][plan_add]"> Add New </label></li>
                                      <li> <label><input type="checkbox"  name="permissions[plantabs][plan_edit]"> Edit </label></li>
                                   
                                    <li>
                                       <label><input type="checkbox" name="permissions[plantabs][plan_delete]">Delete</label></li>
                                   </ul>
                                </div>
                                  
                                </div>
                              
                            </div>
 <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="payments_permision" name="permissions['paymentstab']"> Payments Tab</label>
                                    </div>
                                    <div class="payments_permision" style="display: none;">
                                  <ul style="list-style-type:none;">
                                     <li><label><input type="checkbox"  class="payment_checkbox" name="permissions[paymentstab][payment_add]"> Add</label></li>
                                 
                                  </ul> 
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="property_permision"  name="permissions[propertytabs]"> Property Tab</label>
                                    </div>
                                    <div class="property_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="property_checkbox" id="residential_permision"  name="permissions[propertytabs][residential_permision]"> Residential </label>
                                          <div class="residential_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox"  class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_new]"> Add New </label></li>
                                   
                                    <li>
                                       <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_edit]">Edit</label></li>
                                       <li>
                                       <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_delete]">Delete</label></li>
                                        <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_activedeactive]">Active / Deactive</label></li>
                                   </ul>
                                </div>

                                      </li>
                                   
                                    <li>
                                       <label><input id="commercial_permision" class="property_checkbox" type="checkbox" name="permissions[propertytabs][commercialtabs]">Commercial</label>
                                        <div class="commercial_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_new]"> Add New </label></li>
                                   
                                    <li>
                                       <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_edit]">Edit</label></li>
                                       <li>
                                       <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_delete]">Delete</label></li>
                                        <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_activedeactive]">Active / Deactive</label></li>
                                   </ul>
                                </div>
                                   </li>
                                   </ul>
                                </div>
                                  
                                </div>
                              
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="ratings_permision"  name="permissions[ratingtabs]"> Rating & Review Tab</label>
                                    </div>
                                    <div class="ratings_permision" style="display: none;">
                                  <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="rating_checkbox" name="permissions[ratingtabs][rating_add]">Add New</label></li>
                                   
                                   
                                       <li><label><input type="checkbox" class="rating_checkbox" name="permissions[ratingtabs][rating_edit]">Edit</label></li>
                                   <li><label><input type="checkbox" class="rating_checkbox" name="permissions[ratingtabs][rating_delete]"> Delete</label></li>
                                      
                                   
                                  <li>
                                     
                                  </ul> 
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="report_permision"  name="permissions[reporttabs]"> Report Management</label>
                                    </div>
                                      <div class="report_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" id="broker_permision" class="reportt_checkbox"   name="permissions[reporttabs][broker]"> Broker & Property </label>
                                          <div class="broker_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                      
                                   
                                    <li>
                                       <label><input type="checkbox" class="report_checkbox"  name="permissions[reporttabs][broker][broker_edit]">Edit</label></li>
                                       <li>
                                       <label><input type="checkbox" class="report_checkbox"  name="permissions[reporttabs][broker][broker_delete]">Delete</label></li>
                                        
                                   </ul>
                                </div>

                                </div>
                              
                            </div>
                            <!-- usuuuus -->

                        </div><!-- /.box-body -->
       <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="external_permision"  name="permissions[externaltabs]"> External Customers Tab</label>
                                    </div>
                                    <div class="external_permision" style="display: none;">
                                  <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="external_checkbox"  name="permissions[externaltabs][external_add]"> Add New</label></li>
                                   
                                   
                                       <li><label><input type="checkbox" class="external_checkbox" name="permissions[externaltabs][external_delete]"> Delete</label></li>
                                  
                                   
                                 
                                  </ul> 
                                </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="settings_permision"  name="permissions[settingtabs]"> Settings Tab</label>
                                    </div>
                                    <div class="settings_permision" style="display: none;">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="setting_checkbox"  name="permissions[settingtabs][setting_edit]"> Edit </label></li>
                                   
                                   
                                   </ul>
                                </div>
                                  
                                </div>
                              
                            </div>
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script type="text/javascript">
    $("#user_permision").click(function(){
  $(".user_permision").toggle();
});
      $("#plan_permision").click(function(){
  $(".plan_permision").toggle();
});
         $("#payments_permision").click(function(){
  $(".payments_permision").toggle();
});
      $("#property_permision").click(function(){
  $(".property_permision").toggle();
});  
       $("#residential_permision").click(function(){
  $(".residential_permision").toggle();
});  
              $("#commercial_permision").click(function(){
  $(".commercial_permision").toggle();
});  
  
        $("#report_permision").click(function(){
  $(".report_permision").toggle();
}); 
   $("#broker_permision").click(function(){
  $(".broker_permision").toggle();
}); 
     $("#external_permision").click(function(){
  $(".external_permision").toggle();
}); 

   $("#settings_permision").click(function(){
  $(".settings_permision").toggle();
});  
 $("#ratings_permision").click(function(){
  $(".ratings_permision").toggle();
});    

</script>
<script type="text/javascript">
$(document).ready(function(){
    $('#user_permision').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.checkbox').on('click',function(){
      
            $('#user_permision').prop('checked',true);
       
    });
});


$(document).ready(function(){
    $('#plan_permision').on('click',function(){
        if(this.checked){
          
        }else{
             $('.plan_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
   
});



$(document).ready(function(){
    $('#payments_permision').on('click',function(){
        if(this.checked){
           
        }else{
             $('.payment_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
});



$(document).ready(function(){
    $('#property_permision').on('click',function(){
        if(this.checked){
           
        }else{
             $('.property_checkbox').each(function(){
                this.checked = false;
            });
              $('.residential_checkbox').each(function(){
                this.checked = false;
            });
               $('.commercial_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
   
});



$(document).ready(function(){
    $('#residential_permision').on('click',function(){
        if(this.checked){
         
        }else{
             $('.residential_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
   
});
$(document).ready(function(){
    $('#commercial_permision').on('click',function(){
        if(this.checked){
        
        }else{
             $('.commercial_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
   
});


$(document).ready(function(){
    $('#ratings_permision').on('click',function(){
        if(this.checked){
           
        }else{
             $('.rating_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    
});


$(document).ready(function(){
    $('#report_permision').on('click',function(){
        if(this.checked){
           
        }else{
             $('.report_checkbox').each(function(){
                this.checked = false;
            });
             $('.reportt_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
   
});
$(document).ready(function(){
    $('#external_permision').on('click',function(){
        if(this.checked){
            
        }else{
             $('.external_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    
});
$(document).ready(function(){
    $('#settings_permision').on('click',function(){
        if(this.checked){
           
        }else{
             $('.setting_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
   
});
</script>