<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>Control panel</small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>150</h3>
                  <p>New Tasks</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php 
                  if(!empty($userRecords['totalpay']))
                  {
                    echo $userRecords['totalpay'];
                  }else{
                    echo 00;
                  }
                 
                  ?><sup style="font-size: 20px"></sup></h3>
                  <p>Total Payment</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                <h3><?php echo $userRecords['userCount'] ?></h3>
                  <p>Total customers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>userListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3>65</h3>
                  <p>Reopened Issue</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
    </section>
    <section class="content">
      <div class="row margin-top-5">
        <div class="col-sm-12">
          <div class="box">
              <!-- <section class="panel form-group-sm">
                  <label class="control-label">Select Account Year:</label>
                  <select class="form-control" id="earningAndExpenseYear">
                      <?php $years = range("2020", date('Y')); ?>
                      <?php foreach($years as $y):?>
                      <option value="<?=$y?>" <?=$y == date('Y') ? 'selected' : ''?>><?=$y?></option>
                      <?php endforeach; ?>
                  </select>
                  <span id="yearAccountLoading"></span>
              </section> -->
              <div id="pie_chart" style="height: 370px; width: 100%;"></div>
          </div>
        </div>
    </section>
    <section class="content">
      <div class="row margin-top-5">
        <!-- <div class="col-sm-4">
          <section class="panel form-group-sm">
              <label class="control-label">Select Account Year:</label>
              <select class="form-control" id="earningAndExpenseYear">
                  <?php $years = range("2020", date('Y')); ?>
                  <?php foreach($years as $y):?>
                  <option value="<?=$y?>" <?=$y == date('Y') ? 'selected' : ''?>><?=$y?></option>
                  <?php endforeach; ?>
              </select>
              <span id="yearAccountLoading"></span>
          </section>
          
          <section class="panel">
            <center>
                <canvas id="paymentMethodChart" width="200" height="200"/></canvas><br>Properties(%)<span id="paymentMethodYear"></span>
            </center>
          </section>
        </div> -->
        <div class="col-sm-12">
            <div class="box">
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>
        </div> 
    </section>
    
</div>
</div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="<?=base_url('assets/js/chart.js'); ?>"></script>
<script src="<?=base_url('assets/js/dashboard.js')?>"></script>
<?php
 
$dataPoints = array(
  array("label"=> "Residential Resale", "y"=> 60.0), 
  array("label"=> "Residential Rent", "y"=> 1.5),
  array("label"=> "Commercial Sale", "y"=> 1.5),
  array("label"=> "Commercial Rent", "y"=> 1.3),
  array("label"=> "Flatmates", "y"=> 0.9),
  array("label"=> "PG/Hostel", "y"=> 0.8)
);
  

?>
<script>
 

window.onload = function () {
 
  var chart = new CanvasJS.Chart("pie_chart", {
    animationEnabled: true,
    exportEnabled: true,
    title:{
      text: "All Properties in 2020"
    },
    subtitles: [{
      text: "Properties"
    }],
    data: [{
      type: "pie",
      showInLegend: "true",
      legendText: "{label}",
      indexLabelFontSize: 16,
      indexLabel: "{label} - #percent%",
      yValueFormatString: "##0",
      dataPoints: <?php echo json_encode($pie_chart, JSON_NUMERIC_CHECK); ?>
    }]
  });
  chart.render();
  var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    theme: "light2",
    title: {
      text: "All Properties - 2020"
    },
    axisY: {
      suffix: "%",
      scaleBreaks: {
        autoCalculate: true
      }
    },
    data: [{
      type: "column",
      yValueFormatString: "#,##0\"%\"",
      indexLabel: "{y}",
      indexLabelPlacement: "inside",
      indexLabelFontColor: "white",
      dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }]
  });
  chart.render(); 
}
</script>