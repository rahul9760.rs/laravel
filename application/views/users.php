<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php
  $permision = unserialize($_SESSION['permision']);
 
            ?>
            <i class="fa fa-users"></i> User Management
            <small>Add, Edit, Delete</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
         <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Users</a></li>
              <li class="breadcrumb-item active" aria-current="page">User list</li>
               <!-- <li class="breadcrumb-item active" aria-current="page">Rent</li> -->
            </ol>
          </nav>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Users List</h3>
                        <div class="box-tools">
                            <form action="<?php echo base_url() ?>userListing" method="POST" id="searchList">
                                <div class="input-group">
                                    <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" />
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile</th>
                                <th>Created On</th>
                                <th>Role</th>
                                <th>Payment History</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                    ?>
                            <tr>
                                <td><?php echo $record->name ?></td>
                                <td><?php echo $record->email ?></td>
                                <td><?php echo $record->mobile ?></td>
                                <td><?php echo $record->created_at; ?></td>
                                <td><?php echo $record->role ?></td>

                                <?php
                                 if(isset($permision['usertabs']['payment_history'])=="on")
                                    {
                                echo '<td><center><a class="btn btn-sm btn-primary" href="'.base_url().'/property/paymentHistory/'.$record->id.'" title="Login history"><i class="fa fa-credit-card"></i></center></a>
                                    </td>';

                                }
                                echo '<td class="text-center">';
                                 if(isset($permision['usertabs']['login_history'])=="on")
                                    {
                                    echo '<a class="btn btn-sm btn-primary" href="'.base_url().'login-history/'.$record->id.'" title="Login history"><i class="fa fa-history"></i></a>';
                                    }
                                      if(isset($permision['usertabs']['mail'])=="on")
                                    {
                                    echo '<a class="btn btn-sm btn-danger emailUser" href="#" data-name="'.$record->name.'" data-mail="'.$record->email.'" data-userid="'.$record->id.'" title="Mail"><i class="fa fa-paper-plane"></i></a> |';
                                  }if(isset($permision['usertabs']['user_edit'])=="on")
                                    {
                                    echo '<a class="btn btn-sm btn-info" href="'.base_url().'editOld/'.$record->id.'" title="Edit"><i class="fa fa-pencil"></i></a>';
                                   }if(isset($permision['usertabs']['user_delete'])=="on")
                                    {
                                    echo '<a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="'.$record->id.'" title="Delete"><i class="fa fa-trash"></i></a>';
                                    }
                                     if($role == ROLE_ADMIN)
                                     {
                                       ?>
                                   
                                
                                    <?php
                                  
                                        echo '<a class="btn btn-sm btn-primary" href="'.base_url().'/property/paymentHistory/'.$record->id.'" title="Login history"><i class="fa fa-credit-card"></i></a></td>';
                                        echo '<td class="text-center">
                                    <a class="btn btn-sm btn-primary" href="'.base_url().'login-history/'.$record->id.'" title="Login history"><i class="fa fa-history"></i></a>
                                    <a class="btn btn-sm btn-danger emailUser" href="#" data-name="'.$record->name.'" data-mail="'.$record->email.'" data-userid="'.$record->id.'" title="Mail"><i class="fa fa-paper-plane"></i></a> |
                                    <a class="btn btn-sm btn-info" href="'.base_url().'editOld/'.$record->id.'" title="Edit"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="'.$record->id.'" title="Delete"><i class="fa fa-trash"></i></a>
                                        </td>';
                                     }else{
                                        echo '</td>';
                                     }
                                       ?>
                                   <td>
                                    
                                </td>
                              
                            
                            </tr>
                            <?php
                        }
                    }
                    ?>
                        </table>

                    </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                        <?php echo $this->pagination->create_links(); ?>
                    </div>
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<!-- Modal -->
<div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Send Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class=""><label for="subject">Enter Subject</label></div>                
                <input type="text" name="email_subject" id="email_subject" class="form-control input-sm" style="width: 150px;" placeholder="Enter Subject">
                <br>
                <div class=""><label for="message">Enter Mesaage</label></div>
                <textarea name="email_message" id="email_message" cols="75" rows="6"></textarea>
                
            </div>
            <div class="modal-footer">
                <button type="button" name="send_mail" id="send_mail" class="btn btn-primary">Send</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('ul.pagination li a').click(function(e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });

</script>
