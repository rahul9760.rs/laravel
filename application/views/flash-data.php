<?php if ($this->session->flashdata('msg')): ?>
    <div class="alert alert-<?php echo $this->session->flashdata('type'); ?>">
      <?php echo $this->session->flashdata('msg'); ?>
    </div>
<?php endif ?>