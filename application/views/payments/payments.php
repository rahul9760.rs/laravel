<div class="content-wrapper">
    <!-- Content Header (Page header) -->
   
      <style type="text/css">
    .invoice-signup-popup .modal-header{
        border:none;
    }
    .invoice-signup-popup p{
        margin-bottom: 0.4em;
    }
    .invoice-signup-popup .modal-content{
        border-radius: inherit;
        background: #FCFCFC;
        border-radius: 4px 4px 4px 4px; box-shadow: 0 0 6px 1px rgba(0,0,0,.3);
    }
    .invoice-signup-popup .close{
        padding: 10px 15px 30px;
    }
    .invoice-signup-popup ul li{
        list-style: none;
    }
       .payment{
            color: green;
            font-family: monospace;
        }
    @media (min-width: 1200px){
    .invoice-signup-popup   .modal-dialog {
        width: 1050px;
        margin: 1.75rem auto;
    }
    .invoice-signup-popup   .modal-footer{
        display: grid;
        grid-template-columns: repeat(6,1fr);
    }   
    .invoice-signup-popup .modal-footer p{
       grid-column: 1 / 5;
        text-align: left
    }
    .invoice-signup-popup .amount-details td:first-child{
        text-align: right;
    }
    .invoice-signup-popup .tax-detail-address{
            display: grid;
        grid-template-columns: 190px 1fr;
        grid-gap: 35px;
    }

 

    .invoice-signup-popup .tax-detail-address ul {
        border-right: 2px solid #000;
    }
}
@media screen {
  #printSection {
      display: none;
  }
}

@media print {
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  #printSection {
    position:absolute;
    left:0;
    top:0;
  }
}


@media screen {
  #printSection {
      display: none;
  }
}

@media print {
  body * {
    visibility:hidden;
  }
  #printSection, #printSection * {
    visibility:visible;
  }
  
}



   </style>
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Payment Management
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('payments/addNewPayment'); ?>"><i class="fa fa-plus"></i> Add New Payment</a>
                </div>
            </div>
        </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Payments</a></li>
              <li class="breadcrumb-item active" aria-current="page">Payment History</li> 
            </ol>
          </nav>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <?php
                    if(!empty($userRecords))
                    {
                        $amount=0;
                        foreach($userRecords as $record)
                        {
                            $amount+=$record->total_amount;
                        }
                        echo "<center><h3 class='payment'>Total Payment: Rs. $amount</h3></center>";
                    }else{
                        echo "<center><h3 class='payment'>00.00</h3></center>";
                    }
                ?>
                <div class="box-header">
                    <h3 class="box-title">Payment List</h3>
                    <div class="box-tools">
                        <form action="" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="example">
                    <tr>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Payment Amount</th>
                        <th>Plan</th>
                        <th>Taxation Id</th>
                        <th>Payment Status</th> 
                        <th>Created On</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($userRecords))
                    {
                        foreach($userRecords as $record)
                        {
                            if($record->status=='success'){ $color= "style=color:green;"; }else{ $color ="style=color:red;" ;}
                           // echo "string";
                           // print_r($record);
                    ?>
                    <tr>
                        <td><?php echo $record->name ?></td>
                        <td><?php echo $record->mobile ?></td>
                        <td><?php echo $record->amount ?></td>
                        <td><?php echo $record->plan_type ?></td>
                         <td class="hide"><?php echo $record->plan_type_selected ?></td>
                          <td class="hide"><?php echo $record->gst ?></td>
                        <td>
                            <?php
                            if($record->razorpay_payment_id!='')
                            {
                                echo $record->razorpay_payment_id;
                            }else{
                            
                                echo "NA";

                            }


                            ?>
                        </td>
                        
                        <td <?php echo $color; ?>><?php echo $record->status ?></td> 
                        <td><?php echo $record->created_at; ?></td>
                        <td class="text-center">
                           <!--   <a class="btn btn-sm btn-info" href="<?php echo base_url().'payments/editOld/'.$record->id; ?>" title="Invoice"><i class="fa fa-address-card-o"></i></a> -->
                           <button type="button" class="btn btn-primary use-address" data-toggle="modal" data-target="#myModal">
<i class="fa fa-address-card-o "></i>
</button>
                           <!--  <a class="btn btn-sm btn-primary" href="" title="Payment history"><i class="fa fa-history"></i></a> | 
                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'payments/editOld/'.$record->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deletePayment" href="#" data-paymentid="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></a> -->
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php //echo //$this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>


<!-- The Modal -->
<div id="content2">
 <div id="printThis">
<div class="modal fade invoice-signup-popup in" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <!-- <div class="modal-header"> -->
        <!-- <h4 class="modal-title">Invoice</h4> -->
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      <!-- </div> -->

      <!-- Modal body -->
      <div class="modal-body">
        <div class="invoice-box">
      <div class="modal-body">
        <h3>Invoice</h3>
        <div class="row">
         <div class="col-lg-6">
            <p><strong>My Zero Broker</strong></p>
            <p>Ground Floor Building 2A,23 &amp; 24<br>
                   AMR tech park internal Rd, Hogansdra<br>
                   Bangaluru, Karnataka 560082
            </p>
         </div>
         <div class="col-lg-6 tax-detail-address">
            <ul>
               <li><p><strong>Invoice Date</strong></p>
               <p id="invoice_date"></p></li>
               
               <li>
               <p><strong>Purchase On</strong></p>
               <p id="purchase_date"></p></li>
              
            </ul>
            <div class="address-company">
               <h4>ATTN</h4>
               <p><strong>Vijay Traders Private Limited</strong></p>
               <p>5/1, Penthouse 01, 6th Floor, Rich Homes Appartment, Richmond Road , Bengaluru Karnataka 560025</p>
            </div>


         </div>

        </div>
        <div class="row">
         <div class="col-lg-12">
            <table class="table table-bordered">
                    <thead>
                        <tr>
                     <th scope="col">Description</th>
                     <th scope="col">Plan</th>
                     <th scope="col">Units</th>
                     <th scope="col">Units Price</th>
                     <th scope="col">Tax</th>
                     <th scope="col">Amount</th>

                        </tr>
                    </thead>
                  <tbody>
                   <tr>
                     <td>GET OWNER DETAILS<br>SALE / RESALE</td>
                     <td id="plan_type"></td>
                     <td>NA</td>
                     <td>NA</td>
                     <td>18 %</td>
                     <td id="amount"></td>
                   </tr>
                   
                   <tr class="amount-details">
                     <td colspan="5" id="">Sub-Total</td>
                     <td id="subtotal"></td>

                   </tr>
                   <tr class="amount-details">
                     <td colspan="5">GST</td>
                     <td class="gst"></td>

                   </tr>
                <!--    <tr class="amount-details">
                     <td colspan="5">SGST</td>
                     <td>16,250.00</td>

                   </tr> -->
                   <tr class="amount-details">
                     <td colspan="5" >Total</td>
                     <td id="total"></td>

                   </tr>
               </tbody>
            </table>
         </div>
        </div>
      </div>
    </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button onclick="getPDF()">PDF</button>
         <button type="button" class="btn btn-success" id="btnPrint" >Print</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "plans/" + value);
            jQuery("#searchList").submit();
        });
    });


</script>
 <script type="text/javascript">  
  

 
     $('.use-address').click(function() {
  var tr = $(this).closest('tr');
  var amount = tr.children('td:eq(2)').text();
  var plan_type = tr.children('td:eq(3)').text();
  var plan_for = tr.children('td:eq(4)').text();
  var gst = tr.children('td:eq(5)').text();
  var purchase_date = tr.children('td:eq(8)').text();
var d = new Date();
var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
var planType = '('+ plan_for +')';
$('#plan_type').html(plan_type + planType);
 $('#amount').html('<i class="fa fa-inr" aria-hidden="true"></i>&nbsp' + amount);
$('#invoice_date').html(strDate);
$('#purchase_date').html(purchase_date);
$('.gst').html('<i class="fa fa-inr" aria-hidden="true"></i>&nbsp' + gst);
$('#subtotal').html(amount);
var totolAmount = parseInt(amount) + parseInt(gst);
if(gst==='00')
{

$('#total').html(amount);
   //get the text from first col of current row
}else{
  $('#total').html('<i class="fa fa-inr" aria-hidden="true"></i>&nbsp'+ totolAmount);
  // console.log(a); //you'll get the actual ids here
}
});


 document.getElementById("btnPrint").onclick = function () {
    printElement(document.getElementById("printThis"));
}

function printElement(elem) {
    var domClone = elem.cloneNode(true);
    
    var $printSection = document.getElementById("printSection");
    
    if (!$printSection) {
        var $printSection = document.createElement("div");
        $printSection.id = "printSection";
        document.body.appendChild($printSection);
    }
    
    $printSection.innerHTML = "";
    $printSection.appendChild(domClone);
    window.print();
}
</script>  



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.min.js"></script>

<script type="text/javascript">
  function getPDF(){

  var doc = new jsPDF('p', 'mm', 'a4');
var elementHTML = $('#myModal').html();
var specialElementHandlers = {
    '#elementH': function (element, renderer) {
        return true;
    }
};
doc.fromHTML(elementHTML, 20, 20, {
    // 'width': 100,
    'elementHandlers': specialElementHandlers
});

// Save the PDF
doc.save('invoice.pdf');

}
</script>