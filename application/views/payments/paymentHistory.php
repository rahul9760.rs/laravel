<div class="content-wrapper">
    <style type="text/css">
        .payment{
            color: green;
            font-family: monospace;
        }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <!-- <small>Payment History</small> -->
      </h1>
    </section>
    
    <section class="content">
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>userListing">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Payment History</li>
  </ol>
</nav>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <?php
                    if(!empty($paymentHistory))
                    {
                        $amount=0;
                        foreach($paymentHistory as $record)
                        {
                            $amount+=$record->total_amount;
                        }
                        echo "<center><h3 class='payment'>Total Payment: Rs. $amount</h3></center>";
                    }else{
                        echo "<center><h3 class='payment'>Total Payment: 00.00</h3></center>";
                    }
                ?>
                <div class="box-header">
                    <h3 class="box-title">Payment List</h3>
                    <div class="box-tools">
                        <form action="" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                        <th>PaymentID</th>
                        <th>Plan Type</th>
                        <th>Plan Total Amount</th>
                        <th>Plan Amount</th>
                        <th>GST Amount</th>
                        <th>Validity</th>
                        <th>Payment Status</th> 
                        <th>Created On</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($paymentHistory))
                    {
                        foreach($paymentHistory as $record)
                        {
                            if($record->status=='success'){ $color= "style=color:green;"; }else{ $color ="style=color:red;" ;}
                    ?>
                    <tr>
                        <td><?php echo $record->razorpay_payment_id; ?></td>
                        <td><?php echo $record->plan_type ?></td>
                        <td><?php echo "Rs. ".$record->total_amount ?></td>
                        <td><?php echo "Rs. ".$record->amount ?></td>
                        <td><?php echo "Rs. ".$record->gst ?></td> 
                        <td <?php echo $color; ?>><?php echo $record->validity." Days" ?></td> 
                        <td><?php echo "Success"; ?></td>
                        <td><?php echo $record->created_at; ?></td>
                        <td class="text-center"> 
                            <a class="btn btn-sm btn-info" href="<?php echo base_url().'payments/editOld/'.$record->id; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deletePayment" href="#" data-paymentid="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    <?php
                        }
                    }else{?>
                        <tr>
                            <td colspan="7"><center>No plan purchased</center></td>
                        </tr>
                    <?php } ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "paymentHistory/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
