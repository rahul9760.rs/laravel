<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> User Management
            <small>Add / Edit User</small>
        </h1>
    </section>

    <section class="content">

        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <?php if($this->session->flashdata('message')){?>
                    <div class="alert alert-<?php echo $this->session->flashdata('type');?>">
                       <?php echo $this->session->flashdata('message');?>
                    </div>
                    <?php } ?>
                    <form role="form" id="addPlan" action="" method="post" role="form">                       
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Plan Name</label>
                                        <input type="text" class="form-control required" value="" id="" name="plan_name" maxlength="128">
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Plan price</label>
                                        <input type="text" class="form-control required" id="" value="" name="plan_price" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Validity</label> 
                                        <select class="form-control" name="validity" id="">
                                            <option value="">Select Validity</option>
                                            <option value="1">1 Month</option>
                                            <option value="3">3 Month</option>
                                            <option value="6"> 6 Month</option>
                                            <option value="12">12 Month</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 specification">
                                    <div class="form-group">
                                        <label for="cpassword">Specification 1</label>
                                        <input type="text" class="form-control required" id="specification" name="plan_specification[]" maxlength="20">
                                    </div>
                                </div>
                                <div class="add_one_more">
                                    
                                </div>
                            </div>
                            
                             
                            <!-- usuuuus -->

                        </div><!-- /.box-body -->
                        <input type="button" class="btn btn-primary add-more" value="Add New Specification" /> 
                        
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    var i=2;
    $("body").on("click",".add-more",function(){ 
        var html = $(".specification").first().clone();
     
          $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-danger remove'>- Remove</a>");
      
        var htm=`<div class="col-md-12 specification" id="spc_`+i+`">
                                    <div class="form-group">
                                        <label for="cpassword">Specification `+i+`</label>
                                        <input type="text" class="form-control required equalTo" id="specification_`+i+`" name="plan_specification[]" maxlength="20">
                                        <span class="remove_sp" id="remove_sp_`+i+`" style="color:red">X</span>
                                    </div>
                                </div>`; 
          $(".add_one_more").append(htm);
      
//     serialize($this->input->post('name'))
       i++;
    });

    $("body").on("click",".remove_sp",function(){ 
        var id=$(this).attr('id');
        var ar=id.split('_');
        var rid=ar[2];
        $('#spc_'+rid).remove();
    });
});


</script>