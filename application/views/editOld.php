<?php
$userId = $userInfo->id;
$name = $userInfo->name;
$email = $userInfo->email;
$mobile = $userInfo->mobile;
$roleId = $userInfo->roleId;

$permision = unserialize($userInfo->permision);
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> User Management
        <!-- <small>Add / Edit User</small> -->
      </h1>
    </section>
    
    <section class="content">
      <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>userListing">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit User</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter User Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editUser" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Full Name</label>
                                        <input type="text" class="form-control" id="fname" placeholder="Full Name" name="fname" value="<?php echo $name; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $userId; ?>" name="userId" id="userId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label>
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="20">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label>
                                        <input type="text" class="form-control" id="mobile" placeholder="Mobile Number" name="mobile" value="<?php echo $mobile; ?>" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Role</label>
                                        <select class="form-control" id="role" name="role">
                                            <option value="0">Select Role</option>
                                            <?php
                                            if(!empty($roles))
                                            {
                                                foreach ($roles as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->id; ?>" <?php if($rl->id == $roleId) {echo "selected=selected";} ?>><?php echo $rl->role ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea  class="form-control" rows="5" id="address" name="address">
                                            <?php echo $userInfo->address; ?>
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">Datge of birth</label>
                                        <input type="text" class="form-control" id="dob" name="dob" value="<?php echo $userInfo->dob; ?>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control" id="country" name="country" value="<?php echo $userInfo->country; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">state</label>
                                        <input type="text" class="form-control" id="state" name="state"value="<?php echo $userInfo->state; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="city">city</label>
                                        <input type="text" class="form-control" id="city" name="city" value="<?php echo $userInfo->city; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value="1" name="get_whats_app_updates">get updates on whatsapp  ?</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                               <nav aria-label="breadcrumb">
                                          <ol class="breadcrumb">
                                            <li class="breadcrumb-item active" aria-current="page">Permissions</li>
                                          </ol>
                                        </nav>
                                                                     <div class="row">
                                <?php
                                   if($permision['usertabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox"
                                       id="user_permision" 
                                       name="permissions[usertabs]"
                                        <?php if ($permision['usertabs']) { echo "checked='checked'"; } ?> 

                                        > User Tab</label>
                                    </div>
                                    <div class="user_permision">
                                  <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="checkbox" name="permissions[usertabs][payment_history]"
                                        <?php if ($permision['usertabs']['payment_history'] == "on") { echo "checked='checked'"; } ?>

                                        > Payment History</label></li>
                                   
                                   
                                       <li><label><input type="checkbox" class="checkbox" name="permissions[usertabs][login_history]"
                                       <?php if ($permision['usertabs']['login_history'] == "on") { echo "checked='checked'"; } ?>
                                        > Login History</label></li>
                                   <li><label><input type="checkbox" class="checkbox" name="permissions[usertabs][mail]"
                                     <?php if ($permision['usertabs']['mail'] == "on") { echo "checked='checked'"; } ?>

                                    > Mail</label></li>
                                      <li> <label><input type="checkbox" class="checkbox" name="permissions[usertabs][user_edit]"
                                         <?php if ($permision['usertabs']['user_edit'] == "on") { echo "checked='checked'"; } ?>

                                        > Edit</label></li>
                                   
                                  <li>
                                       <label><input type="checkbox" class="checkbox"  name="permissions[usertabs][user_delete]"
                                         <?php if ($permision['usertabs']['user_delete'] == "on") { echo "checked='checked'"; } ?>
                                        > Delete</label></li>
                                  </ul> 
                                </div>
                                </div>
                                <?php
                                  }
                                ?>
                                 <?php
                                   if($permision['plantabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="plan_permision"  name="permissions[plantabs]"
                                         <?php if ($permision['plantabs']) { echo "checked='checked'"; } ?>

                                        > Plans Tab</label>
                                    </div>
                                    <div class="plan_permision">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="plan_checkbox"  name="permissions[plantabs][plan_edit]"
                                         <?php if ($permision['plantabs']['plan_edit'] == "on") { echo "checked='checked'"; } ?>
                                        > Edit </label></li>
                                   
                                    <li>
                                       <label><input type="checkbox" class="plan_checkbox" name="permissions[plantabs][plan_delete]"
                                         <?php if ($permision['plantabs']['plan_delete'] == "on") { echo "checked='checked'"; } ?>
                                        >Delete</label></li>
                                   </ul>
                                </div>
                                  
                                </div>
                              <?php
                                  }
                              ?>
                            </div>
 <div class="row">
      <?php
                                   if($permision['paymentstab'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="payments_permision" name="permissions['paymentstab']"
                                        <?php if ($permision['paymentstab']) { echo "checked='checked'"; } ?>
                                        > Payments Tab</label>
                                    </div>
                                    <div class="payments_permision" >
                                  <ul style="list-style-type:none;">
                                     <li><label><input type="checkbox" class="payment_checkbox" name="permissions[paymentstab][payment_add]"
                                         <?php if ($permision['paymentstab']['payment_add'] == "on") { echo "checked='checked'"; } ?>

                                        > Add</label></li>
                                 
                                  </ul> 
                                </div>
                                </div>
                                <?php
                                     }
                                ?>
                                <?php
                                   if($permision['propertytabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="property_permision"  name="permissions[propertytabs]"
                                         <?php if ($permision['propertytabs']) { echo "checked='checked'"; } ?>

                                        > Property Tab</label>
                                    </div>
                                     <?php
                                   if($permision['propertytabs']['residential_permision'])
                                   {
                                ?>
                                    <div class="property_permision" >
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="property_checkbox" id="residential_permision"  name="permissions[propertytabs][residential_permision]"
                                         <?php if ($permision['propertytabs']['residential_permision']) { echo "checked='checked'"; } ?>

                                        > Residential </label>
                                          <div class="residential_permision">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_new]"
                                         <?php if ($permision['propertytabs']['residential_permision']['residential_new'] == "on") { echo "checked='checked'"; } ?>

                                        > Add New </label></li>
                                   
                                    <li>
                                       <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_edit]"
                                         <?php if ($permision['propertytabs']['residential_permision']['residential_edit'] == "on") { echo "checked='checked'"; } ?>

                                        >Edit</label></li>
                                       <li>
                                       <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_delete]"
                                         <?php if ($permision["propertytabs"]["residential_permision"]["residential_delete"] == "on") { echo "checked='checked'"; } ?>

                                        >Delete</label></li>
                                        <label><input type="checkbox" class="residential_checkbox" name="permissions[propertytabs][residential_permision][residential_activedeactive]"
                                             <?php if ($permision["propertytabs"]["residential_permision"]["residential_activedeactive"] == "on") { echo "checked='checked'"; } ?>

                                            >Active / Deactive</label></li>
                                   </ul>
                                </div>
                                <?php
                            }

                                ?>

                                      </li>
                                    <?php
                                   if($permision['propertytabs']['commercialtabs'])
                                   {
                                ?>
                                    <li>
                                       <label><input id="commercial_permision" class="property_checkbox" type="checkbox" name="permissions[propertytabs][commercialtabs]"
                                         <?php if ($permision["propertytabs"]["commercialtabs"]) { echo "checked='checked'"; } ?>
                                        >Commercial</label>
                                        <div class="commercial_permision">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="commercial_checkbox"  name="permissions[propertytabs][commercialtabs][commercial_new]"
                                         <?php if ($permision["propertytabs"]["commercialtabs"]["commercial_new"] == "on") { echo "checked='checked'"; } ?>
                                        > Add New </label></li>
                                   
                                    <li>
                                       <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_edit]"
                                         <?php if ($permision["propertytabs"]["commercialtabs"]["commercial_edit"] == "on") { echo "checked='checked'"; } ?>

                                        >Edit</label></li>
                                       <li>
                                       <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_delete]"
                                         <?php if ($permision["propertytabs"]["commercialtabs"]["commercial_delete"] == "on") { echo "checked='checked'"; } ?>
                                        >Delete</label></li>
                                        <label><input type="checkbox" class="commercial_checkbox" name="permissions[propertytabs][commercialtabs][commercial_activedeactive]"
                                            <?php if ($permision["propertytabs"]["commercialtabs"]["commercial_activedeactive"] == "on") { echo "checked='checked'"; } ?>
                                            >Active / Deactive</label></li>
                                        <?php
                                    }

                                        ?>
                                   </ul>
                                </div>
                                   </li>
                                   </ul>
                                </div>
                                  
                                </div>
                              <?php
                                   }
                              ?>
                            </div>
                             <div class="row">
                                 <?php
                                   if($permision['ratingtabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="ratings_permision"  name="permissions[ratingtabs]"
                                        <?php if ($permision["ratingtabs"]) { echo "checked='checked'"; } ?>
                                        > Rating & Review Tab</label>
                                    </div>
                                    <div class="ratings_permision">
                                  <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="rating_checkbox" name="permissions[ratingtabs][rating_add]"
                                        <?php if ($permision["ratingtabs"]["rating_add"] == "on") { echo "checked='checked'"; } ?>
                                        >Add New</label></li>
                                   
                                   
                                       <li><label><input type="checkbox" class="rating_checkbox" name="permissions[ratingtabs][rating_edit]"
                                        <?php if ($permision["ratingtabs"]["rating_edit"] == "on") { echo "checked='checked'"; } ?>
                                        >Edit</label></li>
                                   <li><label><input type="checkbox" class="rating_checkbox"  name="permissions[ratingtabs][rating_delete]"
                                    <?php if ($permision["ratingtabs"]["rating_delete"] == "on") { echo "checked='checked'"; } ?>
                                    > Delete</label></li>
                                      
                                   
                                  <li>
                                     
                                  </ul> 
                                </div>
                                </div>
                                <?php
                            }

                                 ?>
                                  <?php
                                   if($permision['reporttabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="report_permision"  name="permissions[reporttabs]"
                                         <?php if ($permision["reporttabs"]) { echo "checked='checked'"; } ?>
                                        > Report Management</label>
                                    </div>
                                      <div class="report_permision">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="reportt_checkbox" id="broker_permision"  name="permissions[reporttabs][broker]"
                                         <?php if ($permision["reporttabs"]["broker"]) { echo "checked='checked'"; } ?>
                                        > Broker & Property </label>
                                          <div class="broker_permision">
                                   <ul style="list-style-type:none;">
                                      
                                   
                                    <li>
                                       <label><input type="checkbox" class="report_checkbox" name="permissions[reporttabs][broker][broker_edit]"
                                         <?php if ($permision["reporttabs"]["broker"]["broker_edit"] == "on") { echo "checked='checked'"; } ?>
                                        >Edit</label></li>
                                       <li>
                                       <label><input type="checkbox" class="report_checkbox" name="permissions[reporttabs][broker][broker_delete]"
                                         <?php if ($permision["reporttabs"]["broker"]["broker_delete"] == "on") { echo "checked='checked'"; } ?>
                                        >Delete</label></li>
                                        
                                   </ul>
                                </div>

                                </div>
                              
                            </div>
                            <?php
                        }

                            ?>
                            <!-- usuuuus -->

                        </div><!-- /.box-body -->
       <div class="row">
          <?php
                                   if($permision['externaltabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="external_permision"  name="permissions[externaltabs]"
                                         <?php if ($permision["externaltabs"]) { echo "checked='checked'"; } ?>
                                        > External Customers Tab</label>
                                    </div>
                                    <div class="external_permision">
                                  <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="external_checkbox" name="permissions[externaltabs][external_add]"
                                         <?php if ($permision["externaltabs"]["external_add"] == "on") { echo "checked='checked'"; } ?>
                                        > Add New</label></li>
                                   
                                   
                                       <li><label><input type="checkbox" class="external_checkbox" name="permissions[externaltabs][external_delete]"
                                         <?php if ($permision["externaltabs"]["external_delete"] == "on") { echo "checked='checked'"; } ?>
                                        > Delete</label></li>
                                  
                                   
                                 
                                  </ul> 
                                </div>
                                </div>
                                <?php
                            }

                                ?>
                                 <?php
                                   if($permision['settingtabs'])
                                   {
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                       <label><input type="checkbox" id="settings_permision"  name="permissions[settingtabs]"
                                          <?php if ($permision["settingtabs"]) { echo "checked='checked'"; } ?>
                                        > Settings Tab</label>
                                    </div>
                                    <div class="settings_permision">
                                   <ul style="list-style-type:none;">
                                      <li> <label><input type="checkbox" class="setting_checkbox" name="permissions[settingtabs][setting_edit]"
                                          <?php if ($permision["settingtabs"]["setting_edit"] == "on") { echo "checked='checked'"; } ?>
                                        > Edit </label></li>
                                   
                                   
                                   </ul>
                                </div>
                                  
                                </div>
                                <?php
                                }
 
                                ?>
                              
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#user_permision').on('click',function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.checkbox').on('click',function(){
      
            $('#user_permision').prop('checked',true);
       
    });
});


$(document).ready(function(){
    $('#plan_permision').on('click',function(){
        if(this.checked){
            $('.plan_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.plan_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.plan_checkbox').on('click',function(){
      
            $('#plan_permision').prop('checked',true);
       
    });
});



$(document).ready(function(){
    $('#payments_permision').on('click',function(){
        if(this.checked){
            $('.payment_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.payment_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.payment_checkbox').on('click',function(){
      
            $('#payments_permision').prop('checked',true);
       
    });
});



$(document).ready(function(){
    $('#property_permision').on('click',function(){
        if(this.checked){
            $('.property_checkbox').each(function(){
                this.checked = true;
            });
            $('.residential_checkbox').each(function(){
                this.checked = true;
            });
              $('.commercial_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.property_checkbox').each(function(){
                this.checked = false;
            });
              $('.residential_checkbox').each(function(){
                this.checked = false;
            });
               $('.commercial_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.property_checkbox').on('click',function(){
      
            $('#property_permision').prop('checked',true);
       
    });
     $('.residential_checkbox').on('click',function(){
      
            $('#property_permision').prop('checked',true);
       
    });
     $('.commercial_checkbox').on('click',function(){
      
            $('#property_permision').prop('checked',true);
       
    });
});



$(document).ready(function(){
    $('#residential_permision').on('click',function(){
        if(this.checked){
            $('.residential_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.residential_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.residential_checkbox').on('click',function(){
      
            $('#residential_permision').prop('checked',true);
       
    });
});
$(document).ready(function(){
    $('#commercial_permision').on('click',function(){
        if(this.checked){
            $('.commercial_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.commercial_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.commercial_checkbox').on('click',function(){
      
            $('#commercial_permision').prop('checked',true);
       
    });
});


$(document).ready(function(){
    $('#ratings_permision').on('click',function(){
        if(this.checked){
            $('.rating_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.rating_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.rating_checkbox').on('click',function(){
      
            $('#ratings_permision').prop('checked',true);
       
    });
});


$(document).ready(function(){
    $('#report_permision').on('click',function(){
        if(this.checked){
            $('.report_checkbox').each(function(){
                this.checked = true;
            });
             $('.reportt_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.report_checkbox').each(function(){
                this.checked = false;
            });
             $('.reportt_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.report_checkbox').on('click',function(){
      
            $('#report_permision').prop('checked',true);
            $('.reportt_checkbox').prop('checked',true);
       
    });
     $('.reportt_checkbox').on('click',function(){
      
            $('#report_permision').prop('checked',true);
           
       
    });
});
$(document).ready(function(){
    $('#external_permision').on('click',function(){
        if(this.checked){
            $('.external_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.external_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.external_checkbox').on('click',function(){
      
            $('#external_permision').prop('checked',true);
       
    });
});
$(document).ready(function(){
    $('#settings_permision').on('click',function(){
        if(this.checked){
            $('.setting_checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.setting_checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.setting_checkbox').on('click',function(){
      
            $('#settings_permision').prop('checked',true);
       
    });
});

</script>