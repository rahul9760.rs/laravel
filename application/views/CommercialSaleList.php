<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Property Management
        <small>Add, Edit, Delete</small>
      
      </h1>
    </section>
    <section class="content">
        <div class="row">
           <!--  <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNewCommercialSale"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div> -->
              <div class="row">
            <?php
            if($role == ROLE_ADMIN)
            {
            echo '<div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="'.base_url('addNewCommercialSale').'"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>';
         }

        ?>
                         <?php

                             if(isset($permision['commercialtabs']['commercial_new'])=="on")
                             {
                            ?>
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url('plans/addNewPlan'); ?>"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <?php
            }

        ?>
        </div>
        </div>
        <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Property</a></li>
    <li class="breadcrumb-item active" aria-current="page">Commercial</li>
     <li class="breadcrumb-item active" aria-current="page">Sale</li>
  </ol>
</nav>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">`
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>CommercialSaleList" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                        <th>Property Type</th>
                        <th>Area</th>
                        <th>Landmark</th>
                        <th>Floor Info</th>
                        <th>City</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    <?php
                    if(!empty($Records))
                    {
                        foreach($Records as $record)
                        {
                    ?>
                    <tr>
                        <td><?php echo $record->property_type ?></td>
                        <td><?php echo $record->area_sq_ft ?></td>
                        <td><?php echo $record->landmark ?></td>
                        <td><?php echo $record->floor_info ?></td>
                        <td><?php echo $record->city ?></td>
                        <td class="text-center">
                             <?php
                              $permision = unserialize($_SESSION['permision']);
                             if(isset($permision['commercialtabs']['commercial_edit'])=="on")
                             {
                                echo ' <a class="btn btn-sm btn-info" href="'.base_url().'editCommercialSaleProperty/'.$record->propertyid.'" title="Edit"><i class="fa fa-pencil"></i></a>';
                             } if(isset($permision['commercialtabs']['commercial_delete'])=="on")
                             {
                                echo '<a class="btn btn-sm btn-danger deleteProperty" href="#" data-propertyid="'.$record->propertyid.'" data-url="deleteCommercialSaleProperty" data-msg="Commercial Sale Property" title="Delete"><i class="fa fa-trash"></i></a>';
                             }
                             
                             if($role == ROLE_ADMIN)
                           {

                            ?>

                              <a class="btn btn-sm btn-info" href="<?php echo base_url().'editCommercialSaleProperty/'.$record->propertyid; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-sm btn-danger deleteProperty" href="#" data-propertyid="<?php echo $record->propertyid; ?>" data-url="deleteCommercialSaleProperty" data-msg="Commercial Sale Property" title="Delete"><i class="fa fa-trash"></i></a>
                                <?php
                            }
                                ?>

                          
                        </td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "editCommercialSaleProperty/" + value);
            jQuery("#searchList").submit();
        });
         jQuery(".property").addClass("menu-open");
       jQuery(".residential").css("display","block");
       jQuery(".commercial-menu").addClass("menu-open");
       jQuery(".commercial-view").css("display","block");
    });
</script>
