<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Plans Management
            <!-- <small>Add / Edit User</small> -->
        </h1>
    </section>

    <section class="content">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
      <?php
        if($plan_name==='Sellers')
        {
          $name = "Seller's Plan";
        }
         if($plan_name==='Buyers')
        {
          $name = "Buyer's Plan";
        }
         if($plan_name==='Owners')
        {
          $name = "Owner's Plan";
        }
         if($plan_name==='Tenants')
        {
          $name = "Tenant's Plan";
        }
      ?>
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>plans">Plan</a></li>
    <li class="breadcrumb-item active" aria-current="page">Individual onwer system</li>
     <li class="breadcrumb-item active" aria-current="page"><?php echo  $name; ?></li>
      <li class="breadcrumb-item active" aria-current="page">Add Plans</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Plan Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <?php $this->load->helper("form"); ?>
                    <?php if($this->session->flashdata('message')){?>
                    <div class="alert alert-<?php echo $this->session->flashdata('type');?>">
                       <?php echo $this->session->flashdata('message');?>
                    </div>
                    <?php }
                 

                     ?>
                    <form role="form" id="addPlan" action="<?php echo base_url() ?>addPlan" method="post" role="form">                       
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fname">Plan Name</label>
                                   
                                        <input type="text" class="form-control required" value="<?php echo $plan_for; ?>" id="" name="plan_name" maxlength="128" readonly>
                                         <input type="hidden" class="form-control required" value="<?php echo $plan_name; ?>" id="" name="plan_for" maxlength="128" readonly>
                                           <input type="hidden" class="form-control required" value="<?php echo $planInfo->id; ?>" id="" name="planId" maxlength="128" readonly>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Plan price</label>
                                       
                                        <input type="text" class="form-control required" id="" value="<?php echo $planInfo->price; ?>" name="plan_price" maxlength="128">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">Validity</label> 
                                           <select class="form-control" name="validity" id="">
                                            <option value=""  >Select</option>
                                            <option value="1" <?php if($planInfo->validity==1){ echo "selected";}  ?>>1 Month</option>
                                            <option value="3" <?php if($planInfo->validity==3){ echo "selected";}  ?>>3 Month</option>
                                            <option value="6" <?php if($planInfo->validity==6){ echo "selected";}  ?>>6 Month</option>
                                            <option value="12" <?php if($planInfo->validity==12){ echo "selected";}  ?>>12 Month</option>
                                        </select>
                                      <!--   <select class="form-control" name="validity" id="">
                                            <option value="">Select Validity</option>
                                            <option value="15">15 days</option>
                                             <option value="1">1 Month</option>
                                            <option value="3">3 Month</option>
                                            <option value="6"> 6 Month</option>
                                            <option value="12">12 Month</option>
                                        </select> -->
                                    </div>
                                </div>
                                 <?php  $aa = unserialize($planInfo->plan_specification); ?>
                                 <?php //echo "<pre>"; print_r($aa); die; ?>
                                <div class="col-md-12 specification">
                                    <div class="form-group">
                                        <label for="cpassword">Specifications</label><br><br>
                                         <div class="col-md-6 col-sm-6">
                                               <div class="formCheckbox">
                                                    <input type="checkbox" name="web_only" 
                                                    value="Website only" <?php if($aa['web_only']){ echo "checked"; } ?>>
                                                    <label for="">

                                                      Website only
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                               <div class="formCheckbox">
                                                    <input type="checkbox" name="privacy" 
                                                    value="100% PRIVACY OF YOUR DATA" <?php if($aa['privacy']){ echo "checked"; } ?>>
                                                    <label for="">

                                                      100% PRIVACY OF YOUR DATA
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                            </div>
                                           <div class="col-md-6 col-sm-6">
                                              
                                                <div class="formCheckbox">
                                                    <input type="checkbox" name="filertation" 
                                                    value="FILTRATION OF PROPERTY" <?php if($aa['filertation']){ echo "checked"; } ?>>
                                                    <label for="">
                                                      FILTRATION OF PROPERTY
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                                <div class="formCheckbox">
                                                    <input type="checkbox" name="alert" 
                                                    value="NEW PROPERTY ALERT ON YOUR MOBILE" <?php if($aa['alert']){ echo "checked"; } ?>>
                                                    <label for="">
                                                     NEW PROPERTY ALERT ON YOUR MOBILE

                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                            </div>
                                             <div class="col-md-6 col-sm-6">
                                              
                                                <div class="formCheckbox">
                                                    <input type="checkbox" name="legal_assistance" 
                                                    value="LEGAL ASSISTANCE" <?php if($aa['legal_assistance']){ echo "checked"; } ?>>
                                                    <label for="LEGAL ASSISTANCE">
                                                      LEGAL ASSISTANCE
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                            </div>
                                              <div class="col-md-6 col-sm-6">
                                               <div class="formCheckbox">
                                                    <input type="checkbox" name="home_loan" 
                                                    value="HOME LOAN ASSISTANCE" <?php if($aa['home_loan']){ echo "checked"; } ?>>
                                                    <label for="">
                                                      HOME LOAN ASSISTANCE
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                                <div class="formCheckbox">
                                                    <input type="checkbox" name="promotion" 
                                                    value="PROPERTY PROMOTION ON DIGITAL MEDIA" <?php if($aa['promotion']){ echo "checked"; } ?>>
                                                    <label for="">
                                                     PROPERTY PROMOTION ON DIGITAL MEDIA
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                            </div>
                                             <div class="col-md-6 col-sm-6">
                                               <div class="formCheckbox">
                                                    <input type="checkbox" name="site_assistance" 
                                                    value="PERSONAL SITE ASSISTANT" <?php if($aa['site_assistance']){ echo "checked"; } ?>>
                                                    <label for="">
                                                      PERSONAL SITE ASSISTANT
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                                <div class="formCheckbox">
                                                    <input type="checkbox" name="photoshoot" 
                                                    value="PHOTOSHOOT OF YOUR PROPERTY" <?php if($aa['photoshoot']){ echo "checked"; } ?>>
                                                    <label for="">
                                                     PHOTOSHOOT OF YOUR PROPERTY
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                            </div>
                                        <div class="col-md-6 col-sm-6">
                                               <div class="formCheckbox">
                                                    <input type="checkbox" name="relationship" 
                                                    value="PERSONAL RELATIONSHIP MANAGER" <?php if($aa['relationship']){ echo "checked"; } ?>>
                                                    <label for="">
                                                      PERSONAL RELATIONSHIP MANAGER
                                                    </label>
                                                    <span class="amenities lift"></span>
                                               </div>
                                               
                                            </div>
                                       
                                    </div>
                                </div>
                                <div class="add_one_more">
                                    
                                </div>
                            </div>
                            
                             
                            <!-- usuuuus -->

                        </div><!-- /.box-body -->
                        <!-- <input type="button" class="btn btn-primary add-more" value="Add New Specification" />  -->
                        
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    var i=2;
    $("body").on("click",".add-more",function(){ 
        var html = $(".specification").first().clone();
     
          $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-danger remove'>- Remove</a>");
      
        var htm=`<div class="col-md-12 specification" id="spc_`+i+`">
                                    <div class="form-group">
                                        <label for="cpassword">Specification `+i+`</label>
                                        <input type="text" class="form-control required equalTo" id="specification_`+i+`" name="plan_specification[]" maxlength="20">
                                        <span class="remove_sp" id="remove_sp_`+i+`" style="color:red">X</span>
                                    </div>
                                </div>`; 
          $(".add_one_more").append(htm);
      
//     serialize($this->input->post('name'))
       i++;
    });

    $("body").on("click",".remove_sp",function(){ 
        var id=$(this).attr('id');
        var ar=id.split('_');
        var rid=ar[2];
        $('#spc_'+rid).remove();
    });
});


</script>