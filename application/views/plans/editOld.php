<?php
$planId = $planInfo->id;
$planname = $planInfo->plan_name;
$planprice = $planInfo->price;
$validity = $planInfo->validity;
$specification = unserialize($planInfo->plan_specification);
// echo "<pre>";
// print_r($specification);
// die;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Plan Management
        <!-- <small>Add / Edit User</small> -->
      </h1>
    </section>
  
    <section class="content">
      <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>plans">Plan</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Plan</li>
  </ol>
</nav>
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Plan Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>plans/editPlan" method="post" id="editUser" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Plan Name</label>
                                        <input type="text" class="form-control" id="" placeholder="Plan Name" name="plan_name" value="<?php echo $planname; ?>" maxlength="128">
                                        <input type="hidden" value="<?php echo $planId; ?>" name="planId" id="planId" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">Plan Price</label>
                                        <input type="text" class="form-control" id="" placeholder="Enter Plan Price" name="price" value="<?php echo $planprice; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="validity">Validity</label>
                                        <select class="form-control" name="validity" id="">
                                            <option value=""  >Select</option>
                                            <option value="1" <?php if($validity==1){ echo "selected";}  ?>>1 Month</option>
                                            <option value="3" <?php if($validity==3){ echo "selected";}  ?>>3 Month</option>
                                            <option value="6" <?php if($validity==6){ echo "selected";}  ?>>6 Month</option>
                                            <option value="12" <?php if($validity==12){ echo "selected";}  ?>>12 Month</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <?php
                               // echo "<pre>";
                               // print_r($specification); die();
                                foreach($specification_list as $key=>$list){ ?>
                                    <div class="col-md-6 col-sm-6">
                                       <div class="formCheckbox">
                                            <input type="checkbox" name="spec[]" value="<?php echo $list->specification; ?>" <?php if(in_array($list->specification, $specification)){ echo "checked"; } ?>>
                                            <label for="<?php echo $key; ?>">
                                                <?php echo $list->specification; ?>
                                            </label>
                                            <span class="amenities lift"></span>
                                       </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                           <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="plan_id">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<script src="<?php echo base_url(); ?>assets/js/editUser.js" type="text/javascript"></script>