<div class="content-wrapper">
<?php
      $this->load->helper('form');
      $error = $this->session->flashdata('error');
      if($error)
      {
  ?>
  <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo $this->session->flashdata('error'); ?>                    
  </div>
  <?php } ?>
  <?php  
      $success = $this->session->flashdata('success');
      if($success)
      {
  ?>
  <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      <?php echo $this->session->flashdata('success'); ?>
  </div>
  <?php } ?>
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <i class="fa fa-home" aria-hidden="true"></i> Property Management
         <small>Add / Edit Property</small>
      </h1>
   </section>
   <section class="content">
      <form data-toggle="validator" role="form" id="ResidentialRentAddProperty" action="<?php echo base_url() ?>ResidentialRentAddProperty" method="post">
         <div class="row">
            <!-- left column -->
            <div  class="col-sm-12">
                 <nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Property</a></li>
    <li class="breadcrumb-item active" aria-current="page">Residential</li>
     <li class="breadcrumb-item active" aria-current="page">Rent</li>
     <li class="breadcrumb-item active" aria-current="page">Add New</li>
  </ol>
</nav>
              <!--  <h3>Left Tabs</h3> -->
               <hr/>
               <div class="col-xs-3">
                  <!-- required for floating -->
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs tabs-left sideways">
                     <li class="active"><a href="#Property-Details" data-toggle="tab">Property Details</a></li>
                     <li><a href="#Locality-Details" data-toggle="tab">Locality Details</a></li>
                     <li><a href="#Rental-Details" data-toggle="tab">Rental Details</a></li>
                     <li><a href="#Gallery" data-toggle="tab">Gallery</a></li>
                     <li><a href="#Amenities" data-toggle="tab">Amenities</a></li>
                     <li><a href="#Schedule" data-toggle="tab">Schedule</a></li>
                  </ul>
               </div>
               <div class="col-xs-9">
                  <!-- Tab panes -->
                  <div class="tab-content">
                     <div class="tab-pane active" id="Property-Details">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="apartment_type">Apartment Type *</label>
                                 <select class="form-control required" id="apartment_type" name="Property[apartment_type]" data-error="Please enter name field." required>
                                    <option>Select</option>
                                    <option value="Apartment">Apartment</option>
                                    <option value="Independent House/Villa">Independent House/Villa</option>
                                    <option value="Gated Community Villa">Gated Community Villa</option>
                                 </select>
                                 <div class="help-block with-errors"></div>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="apartment_name">Apartment Name *</label>
                                 <input type="text" class="form-control" id="apartment_name" name="Property[apartment_name]" required>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="address">BHK Type *</label>
                                 <select class="form-control required" id="bhk_type" name="Property[bhk_type]" required>
                                    <option>Select</option>
                                    <option value="RK1">1 RK</option>
                                    <option value="1">1 BHK</option>
                                    <option value="2">2 BHK</option>
                                    <option value="3">3 BHK</option>
                                    <option value="4">4 BHK</option>
                                    <option value="4+">4+ BHK</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="floor">Floor *</label>
                                 <select class="form-control required" id="floor" name="Property[floor]" required>
                                    <option>Select</option>
                                    <option value="0">Ground</option>
                                 <?php
                                  
                                    if(!empty($floor)){
                                      foreach ($floor as $value){ ?>
                                      <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                     <?php }
                                    }
                                 ?>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="top_floor">Total Floor *</label>
                                 <select class="form-control required" id="top_floor" name="Property[top_floor]" required >
                                    <option>Select</option>
                                    <option value="0">Ground Only</option>
                              <?php
                                 if(!empty($top_floor)){
                                   foreach ($top_floor as $value){ ?>
                                   <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                  <?php }
                                 }
                              ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="property_age">Property Age *</label>
                                 <select class="form-control required" id="property_age" name="Property[property_age]" required>
                                    <option>Select</option>
                                    <option value="Under Construction">Under Construction</option>
                                    <option value="Less than one year">Less than one year</option>
                                    <option value="1-3 Years">1-3 Years</option>
                                    <option value="3-5 Years">3-5 Years</option>
                                    <option value="5-10">5-10 Years</option>
                                    <option value="More than 10 Years">More than 10 Years</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="facing">Facing *</label>
                                 <select class="form-control required" id="facing" name="Property[facing]" required>
                                 <option>Select</option>
                                 <option value="">Select</option>
                                 <option value="North">North</option>
                                 <option value="South">South</option>
                                 <option value="East">East</option>
                                 <option value="West">West</option>
                                 <option value="North-East">North-East</option>
                                 <option value="SE">South-East</option>
                                 <option value="North-West">North-West</option>
                                 <option value="South-West">South-West</option>
                                 <option value="Don't Know">Don't Know</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-4">
                              <div class="form-group" >
                                 <label for="property_size">Property Size *</label>
                                 <input type="text" class="form-control" id="property_size" name="Property[area_sq_ft]" required>
                                 <div class="prpty_append">Sq ft</div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Locality  -->
                     <div class="tab-pane" id="Locality-Details">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="city">City *</label>
                                 <input type="text" class="form-control" id="city" name="Locality[city]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="locality">Locality *</label>
                                 <input type="text" class="form-control" id="locality" name="Locality[locality]" required>
                                 <input type="hidden" id="address" name="city2" />
                                 <input type="hidden" id="cityLat" name="Locality[locality_lat]" />
                                 <input type="hidden" id="cityLng" name="Locality[locality_long]" />
                                 <input type="hidden" name="sale" value="sale" /> 
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="street_addres">Street Addres *</label>
                                 <input type="text" class="form-control" id="street_addres" name="Locality[street_addres]" required>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Rental-Details -->
                     <div class="tab-pane" id="Rental-Details">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <!-- Default inline 1-->
                                  <label for="is_available_for_lease">Is Available For Lease ? *</label>
                                 <div class="custom-control custom-radio custom-control-inline">
                                   <input type="radio" class="custom-control-input" id="AvailableForLeaseYes" name="Rental[is_available_for_lease]" value="Yes" checked>
                                   <label class="custom-control-label" for="AvailableForLeaseYes">Yes</label>

                                   <input type="radio" class="custom-control-input" id="AvailableForLeaseNo" name="Rental[is_available_for_lease]" value="No" >
                                   <label class="custom-control-label"  for="AvailableForLeaseNo">No</label>
                                 </div>
                                 <!-- <input type="text" class="form-control" id="is_available_for_lease" name="Rental[is_available_for_lease]" required> -->
                              </div>
                           </div>
                           <div class="col-md-6 expected_lease_amount">
                              <div class="form-group">
                                 <label for="expected_lease_amount">Expected Lease Amount *</label>
                                 <input type="text" class="form-control" id="expected_lease_amount" name="Rental[expected_rent]">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="expected_depost">Expected Depost *</label>
                                 <input type="text" class="form-control" id="expected_depost" name="Rental[expected_deposit]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                <label class="custom-control-label" for="is_negotiable">Is Negotiable *</label>
                                 <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="price_negotiable" value="Yes" name="Rental[price_negotiable]">
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="maintenance">Maintenance *</label>
                                 <select class="form-control required" id="maintenance" name="Rental[maintenance]" required>
                                    <option value="">Select</option>
                                    <option value="Maintenance Included">Maintenance Included</option>
                                    <option value="Maintenance Extra">Maintenance Extra</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="availablle_from">Availablle From *</label>
                                 <input type="text" class="form-control datetimepicker" id="availablle_from" name="Rental[available_from]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="preferred_tenants">Preferred Tenants *</label>
                                 <select class="form-control required" id="preferred_tenants" name="Rental[preferred_tenants]" required>
                                    <option value="">Select</option>
                                    <option value="Doesn Not Matter">Doesn Not Matter</option>
                                    <option value="Family">Family</option>
                                    <option value="Bachelors">Bachelors</option>
                                    <option value="Company">Company</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="furnishing">Furnishing *</label>
                                 <select class="form-control required" id="furnishing" name="Rental[furnishing]" required>
                                    <option value="">Select</option>
                                    <option value="Fully furnished">Fully furnished</option>
                                    <option value="Semi-furnished">Semi-furnished</option>
                                    <option value="Unfurnished">Unfurnished</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="parking">Parking *</label>
                                 <select class="form-control" id="parking" name="Rental[parking]" required>
                                    <option value="">Select</option>
                                    <option value="Bike">Bike</option>
                                    <option value="Car">Car</option>
                                    <option value="Bike and Car">Bike and Car</option>
                                    <option value="None">None</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="description">Description </label>
                                 <textarea class="form-control" id="description" name="Rental[description]">
                                 </textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- Gallery -->
                     <div class="tab-pane" id="Gallery">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="input-group">
                                 <div class="input-group-prepend">
                                    <span class="input-group-text" id="upload_images01">Upload</span>
                                 </div>
                                 <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="upload_images"
                                       aria-describedby="upload_images01" name="Gallery[upload_images]">
                                    <label class="custom-file-label" for="upload_images">Choose file</label>
                                 </div>
                              </div>
                           </div>
                           <!-- <div class="col-md-6">
                              <div class="form-group">
                                  <label for="dob">Apartment Name *</label>
                                  <input type="text" class="form-control" id="dob" name="dob" >
                              </div>
                              </div> -->
                        </div>
                     </div>
                     <!-- Amenities -->
                     <div class="tab-pane" id="Amenities">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="bathrooms">Bathrooms *</label>
                                 <input type="text" class="form-control" id="bathrooms" name="Amenities[bathrooms]" required>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="water_supply">Water Supply *</label>
                                 <select class="form-control required" id="water_supply" name="Amenities[water_supply]" required>
                                    <option value="">Select</option>
                                    <option value="Corporation">Corporation</option>
                                    <option value="Borewell">Borewell</option>
                                    <option value="Both">Both</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="gym">Gym *</label>
                                 <select class="form-control required" id="gym" name="Amenities[gym]" required>
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="non_veg_allowed">Non Veg. Allowed *</label>
                                 <select class="form-control required" id="non_veg_allowed" name="Amenities[non_veg_allowed]" required>
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="gated_security">Gated Security *</label>
                                 <select class="form-control" id="gated_security" name="Amenities[gated_security]" required>
                                    <option value="">Select</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="who_will_show_the_house">Who Will Show The House *</label>
                                 <select class="form-control" id="who_will_show_the_house" name="Amenities[who_will_show_the_house]" required>
                                    <option value="">Select</option>
                                    <option value="I will show">I will show</option>
                                    <option value="Need Help">Need Help</option>
                                    <option value="Neighbours">Neighbours</option>
                                    <option value="Others">Others</option>
                                    <option value="Security">Security</option>
                                    <option value="Tenants">Tenants</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="secondary_number">Secondary Number *</label>
                                 <input type="text" class="form-control" id="secondary_number" name="Amenities[secondary_number]" required>
                              </div>
                           </div>
                          <!--  <div class="col-md-6">
                              <div class="form-group" >
                                 <label for="select_the_amenities_available">Select The Amenities Available *</label>
                                 <input type="text" class="form-control" id="select_the_amenities_available" name="Amenities[select_the_amenities_available]" required>
                              </div>
                           </div> -->
                        <div class="col-md-12 col-sm-12">
                           <div class="formLabel margin-bottom-20">Select the amenities available </div>
                           <div class="row">
                        <?php 
                           $checkarr= array(
                               'Lift'=>array('Lift','fa-square'),
                                'Internet Services' => array('Internet Services','fa-internet-explorer'),
                                'Air Conditioner' => array('Air Conditioner','fa-window-maximize'),
                                'Club House' => array('Club House','fa-cc-diners-club'),
                                'Intercom' =>array('Intercom','fa-american-sign-language-interpreting'),
                                'Swimming Pool' => array('Swimming Pool','fa-bath'),
                                'Childrens Play Area' => array("Childrens Play Area",'fa-futbol-o'),
                                'Fire Safety' => array('Fire Safety','fa-fire-extinguisher'),
                                'Servant Room' => array('Servant Room','fa-child'),
                                'Shopping Center' => array('Shopping Center','fa-shopping-cart'),
                                'Gas Pipeline' => array('Gas Pipeline','fa-sun-o'),
                                'Park' => array('Park','fa-tree'),
                                'Rain Water Harvesting' => array('Rain Water Harvesting','fa-cloud'),
                                'Sewage Treatment Plant' => array('Sewage Treatment Plant','fa-medkit'),
                                'House Keeping' => array('House Keeping','fa-female'),
                                'Power Backup' => array('Power Backup','fa-battery-full'),
                                'Visitor Parking' => array('Visitor Parking','fa-product-hunt')
                            );
                         foreach($checkarr as $key=>$check){
                          ?>
                            <div class="col-md-6 col-sm-6">
                               <div class="formCheckbox">
                                  <input type="checkbox" name="amenitiesarr[]" value="<?php echo $key; ?>" id="<?php echo $key; ?>">
                                  <i class="fa <?php echo $check[1]; ?>" aria-hidden="true"></i>
                                  <label for="<?php echo $key; ?>"><?php echo $check[0]; ?></label>
                                  <span class="amenities lift"></span>
                               </div>
                            </div>
                         <?php }
                         ?>
                        </div>
                        </div>
                     </div>
                  </div>
                     <!-- Schedule -->
                     <div class="tab-pane" id="Schedule">
                        <div class="row">
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="availability">Availability *</label>
                                 <select class="form-control" id="availability" name="Schedule[availability]" required>
                                    <option value="EVERYDAY">Everyday (Monday - Sunday)</option>
                                    <option value="WEEKDAY">Weekdays (Monday - Friday)</option>
                                    <option value="WEEKEND">Weekends (Saturday - Sunday)</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group">
                                 <label for="start_time">Start Time *</label>
                                 <input type="text" class="form-control timepicker" id="start_time" name="Schedule[start_time]" required>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4">
                              <div class="form-group">
                                 <label for="end_time">End Time *</label>
                                 <input type="text" class="form-control timepicker" id="end_time" name="Schedule[end_time]" required>
                              </div>
                           </div>
                          <!--  <div class="col-md-4">
                              <div class="form-group">
                                 <label for="available_all_day">Available All Day *</label>
                                 <div class="custom-control custom-checkbox">
                                     <input type="checkbox" value="true" id="available_all_day" name="Schedule[available_all_day]" required>
                                 </div>
                              </div>
                           </div> -->
                        </div>
                     </div>
                  </div>
               </div>
               <!--  <div class="clearfix"></div> -->
            </div>
         </div>
         <div class="box-footer">
            <input type="submit" class="btn btn-primary" value="Submit" />
            <input type="reset" class="btn btn-default" value="Reset" />
         </div>
      </form>
   </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery("a[href='http://localhost/laravelschool/ResidentialRentList']").addClass("active");
        jQuery("a[href='http://localhost/laravelschool/ResidentialRentList']").css("color","#fff");
         jQuery(".property").addClass("menu-open");
       jQuery(".residential").css("display","block");
       jQuery(".residential-menu").addClass("menu-open");
       jQuery(".residential-view").css("display","block");
      
         jQuery('#AvailableForLeaseNo').click(function(){
     jQuery(".expected_lease_amount").hide();
  });
 jQuery('#AvailableForLeaseYes').click(function(){
     jQuery(".expected_lease_amount").show();
  });


    });
</script>

