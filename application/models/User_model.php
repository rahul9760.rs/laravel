<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : User_model (User Model)
 * User model class to get to handle user related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User_model extends CI_Model
{   
    // public function getYearEarnings($year=""){
    //     $year_to_fetch = $year ? $year : date('Y');
        
         
    //     $this->db->select('transDate, totalPrice');
    //     $this->db->where(['YEAR(transDate)'=>$year_to_fetch]);
    //     $run_q = $this->db->get('transactions');
        
        
    //     if($run_q->num_rows()){
    //         return $run_q->result();
    //     }
        
    //     else{
    //         return FALSE;
    //     }
    // }
    public function getPaymentMethods($year){
        
         
 

        $this->db->select('apartment_type,"residential_rent" as property_type');
        $this->db->from('resident_rent_property_details');
        $query1 = $this->db->get_compiled_select();

        $this->db->select('apartment_type,"residential_resale" as residential_resale');
        $this->db->from('resident_resale_property_details');
        $query2 = $this->db->get_compiled_select();

        $this->db->select('select_the_type_of_rooms,"residential_pg" as residential_pg');
        $this->db->from('resident_pg_room_details');
        $query3 = $this->db->get_compiled_select();

        $this->db->select('property_type,"commercial_rent" as commercial_rent');
        $this->db->from('commercial_rent_property_details');
        $query4 = $this->db->get_compiled_select();

        $this->db->select('property_type,"commercial_sale" as commercial_sale');
        $this->db->from('commercial_sale_property_details');
        $query5 = $this->db->get_compiled_select();

        $this->db->select('apartment_type,"residential_flatmates" as residential_flatmates');
        $this->db->from('resident_flatmates_property_details');
        $query6 = $this->db->get_compiled_select();

        $run_q = $this->db->query(
                    $query1 . ' UNION ' . 
                    $query2 . ' UNION ' . 
                    $query3 . ' UNION ' . 
                    $query4 . ' UNION ' . 
                    $query5 . ' UNION ' . 
                    $query6 . 'where YEAR(created_at)="2020"
                ')->result();

        
        // die($this->db->last_query());
        if(!empty($run_q)){
            return $run_q;
        }
        
        else{
            return FALSE;
        }
    }
    function userListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, Role.role');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Role', 'Role.id = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return $query->num_rows();
    }

    function countdashboard()
    {
         $query = $this->db->query('SELECT * FROM users');
         $userCount = $query->num_rows();


        
         $this->db->select_sum('amount');
          $result = $this->db->get('mzb_payments')->row();  
          $totalpay = $result->amount;
           $arr = array('userCount' => $userCount, 'totalpay' =>$totalpay);    
   
    return $arr;
    }
    
    function userListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.*, Role.role');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Role', 'Role.id = BaseTbl.roleId','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        // die($this->db->last_query());
        $result = $query->result();        
        return $result;
    }
    function customersListCount($searchText = '',$tab)
    {
     
        if($tab==='etc'){
          $where = '(tab_name="extra_services")';
        }
        if($tab==='whymzb'){
             $where = '(tab_name="why_myzerobroker")';
        }
        $this->db->select('BaseTbl.*');
        $this->db->from('mzb_get_more_details_users as BaseTbl'); 
        $this->db->where($where);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    function customersList($searchText = '', $page, $segment,$tab)
    {
  //       $this->db->where('wrk_fld_exc',140);
  // $this->db->where('wrk_cs_sts','open');
  // $where = '(wrk_dlvrd_sts="open" or wrk_cl_sts = "Success")';
  // $this->db->where($where);
   
        if($tab==='etc'){
          $where = '(tab_name="extra_services")';
        }
        if($tab==='whymzb'){
             $where = '(tab_name="why_myzerobroker")';
        }
        $this->db->select('BaseTbl.*');
        $this->db->from('mzb_get_more_details_users as BaseTbl'); 
        $this->db->where($where);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

      function serviceCustomersListCount($searchText = '',$service)
    {
     
        $where = '(service_name="'.$service.'")';
        $this->db->select('BaseTbl.*');
        $this->db->from('mzb_get_more_details_users as BaseTbl'); 
        $this->db->where($where);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
      function serviceCustomersList($searchText = '', $page, $segment,$service)
    {
        $where = '(service_name="'.$service.'")';
        $this->db->select('BaseTbl.*');
        $this->db->from('mzb_get_more_details_users as BaseTbl'); 
        $this->db->where($where);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    function reportedBrokersCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, reported.*, ');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Role', 'Role.id = BaseTbl.roleId','left');
        $this->db->join('mzb_reported_users as reported', 'reported.userID = BaseTbl.id','INNER');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1); 
        $query = $this->db->get();
        // die($this->db->last_query());
        return $query->num_rows();
    }
    
    function reportedBrokers($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.createdDtm, reported.* 
        , reported.reported_by as reporter_id, reportedby.name as reporter_name');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Role', 'Role.id = BaseTbl.roleId','left');
        $this->db->join('mzb_reported_users as reported', 'reported.userID = BaseTbl.id','INNER');
        $this->db->join('users as reportedby', 'reportedby.id = reported.reported_by','INNER');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->where('BaseTbl.roleId !=', 1); 
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        // echo "<pre>"; print_r($result); die;
        return $result;
    } 

    
    function getUserRoles()
    {
        $this->db->select('id, role');
        $this->db->from('roles');
        $this->db->where('id !=', 1);
        $query = $this->db->get();
        
        return $query->result();
    }

   function  getEditRoles($userId)
   {
        $this->db->select('*');
        $this->db->from('roles');
        $this->db->where('id', $userId);
        $query = $this->db->get();
         return $query->result();
        
   }

      function getRoles()
    {
        $this->db->select('*');
        $this->db->from('roles');
        $query = $this->db->get();
        
        return $query->result();
    }

     
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("users");
        $this->db->where("email", $email);   
        $this->db->where("isDeleted", 0);
        if($userId != 0){
            $this->db->where("id !=", $userId);
        }
        $query = $this->db->get();

        return $query->result();
    }
    
    
     
    function addNewUser($userInfo)
    {
        $this->db->trans_start();
        $this->db->insert('users', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

     function addnew_role($userInfo)
    {
        $this->db->trans_start();
        $save_role = $this->db->insert('roles', $userInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $save_role;
    }
    
    
     
    function getUserInfo($userId)
    {
        //$this->db->select('id, name, email, mobile, roleId');
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('isDeleted', 0);
		$this->db->where('roleId !=', 1);
        $this->db->where('id', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    
     
    function editUser($userInfo, $userId)
    {
        $this->db->where('id', $userId);
        $this->db->update('users', $userInfo);
        
        return TRUE;
    }
    
      
    function update_role($userInfo, $userId)
    {
        $this->db->where('id', $userId);
        $this->db->update('roles', $userInfo);
        
        return TRUE;
    }
    
     
    function deleteUser($userId, $userInfo)
    {

        $this->db->where('id', $userId);
        $this->db->delete('users');
        // $this->db->where('id', $userId);
        // $this->db->update('users', $userInfo);
        
        return $this->db->affected_rows();
    }
function delete_role($userId)
    {

        $this->db->where('id', $userId);
        $this->db->delete('roles');
        // $this->db->where('id', $userId);
        // $this->db->update('users', $userInfo);
        
        return $this->db->affected_rows();
    }

     
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('id, password');
        $this->db->where('id', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('users');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
     
    function changePassword($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('users', $userInfo);
        
        return $this->db->affected_rows();
    }


     
    function loginHistoryCount($userId, $searchText, $fromDate, $toDate)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.userId', $userId);
        }
        $this->db->from('last_login as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }

     
    function loginHistory($userId, $searchText, $fromDate, $toDate, $page, $segment)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.sessionData, BaseTbl.machineIp, BaseTbl.userAgent, BaseTbl.agentString, BaseTbl.platform, BaseTbl.createdDtm');
        $this->db->from('last_login as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.sessionData  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        if(!empty($fromDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) >= '".date('Y-m-d', strtotime($fromDate))."'";
            $this->db->where($likeCriteria);
        }
        if(!empty($toDate)) {
            $likeCriteria = "DATE_FORMAT(BaseTbl.createdDtm, '%Y-%m-%d' ) <= '".date('Y-m-d', strtotime($toDate))."'";
            $this->db->where($likeCriteria);
        }
        if($userId >= 1){
            $this->db->where('BaseTbl.userId', $userId);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

     
    function getUserInfoById($userId)
    {
        $this->db->select('id, name, email, mobile, roleId');
        $this->db->from('users');
        $this->db->where('isDeleted', 0);
        $this->db->where('id', $userId);
        $query = $this->db->get();
        
        return $query->row();
    }

     
    function getUserInfoWithRole($userId)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name, BaseTbl.mobile, BaseTbl.roleId, Roles.role');
        $this->db->from('users as BaseTbl');
        $this->db->join('roles as Roles','Roles.id = BaseTbl.roleId');
        $this->db->where('BaseTbl.id', $userId);
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->row();
    }

}

  