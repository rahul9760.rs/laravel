<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : User_model (User Model)
 * User model class to get to handle user related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Payment_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
   public function Add($table, $data){
       
       if($this->db->insert($table, $data)){
           
           return true;
       }else{
           
           return false;
       }
       
   }
    
     function paymentListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('mzb_my_plans as BaseTbl');
        $this->db->join('users as u','u.id=BaseTbl.userID');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function paymentListing($searchText = '', $page, $segment)
    {
        $this->db->select('BaseTbl.*,u.name,u.mobile');
        $this->db->from('mzb_my_plans as BaseTbl');
        $this->db->join('users as u','u.id=BaseTbl.userID');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
    

      /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deletePayment($paymentId)
    {
        $this->db->where('id', $paymentId);
        $this->db->delete('mzb_payments');
        
        return $this->db->affected_rows();
    }
}

  
