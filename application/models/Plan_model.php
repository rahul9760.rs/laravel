<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : User_model (User Model)
 * User model class to get to handle user related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Plan_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
   public function Add($table, $data){
       
       if($this->db->insert($table, $data)){
           
           return true;
       }else{
           
           return false;
       }
       
   }
    
     function planListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('mzb_plans as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */
    function planListing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('mzb_plans as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
        /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getPlanInfo($plan_name,$plan_for)
    {
        //$this->db->select('id, name, email, mobile, roleId');
        $this->db->select('*');
        $this->db->from('mzb_plans');
        $this->db->where('plan_name', $plan_for);
         $this->db->where('plan_for', $plan_name);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    function editPlan($planInfo, $planId)
    {
        $this->db->where('id', $planId);
        $this->db->update('mzb_plans', $planInfo);
        
        return TRUE;
    }
    
    public function get_all_plan_specifications($plan_name,$plan_for)
    {
        $this->db->select('plan_specification');
        $this->db->from('mzb_plans');
        $this->db->where('plan_name', $plan_for);
         $this->db->where('plan_for', $plan_name);
        $query = $this->db->get();
        
        return $query->row();
    }
    function deletePlan($planId)
    {
        $this->db->where('id', $planId);
        $this->db->delete('mzb_plans');
        
        return $this->db->affected_rows();
    }
   
   function addPlan($data)
   {
    $table = "mzb_plans";
     if($this->db->insert($table, $data)){
           
           return true;
       }else{
           
           return false;
       }
   }

}

  