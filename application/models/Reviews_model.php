<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : User_model (User Model)
 * User model class to get to handle user related data 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Reviews_model extends CI_Model
{
    /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
   public function Add($table, $data){
        
       if($this->db->insert($table, $data)){
           
           return true;
       }else{
           
           return false;
       }
       
   }
    
     function reviewsListingCount($searchText = '')
    {
        $this->db->select('*');
        $this->db->from('mzb_reviews as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    
    function reviewsListing($searchText = '', $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('mzb_reviews as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.email  LIKE '%".$searchText."%'
                            OR  BaseTbl.name  LIKE '%".$searchText."%'
                            OR  BaseTbl.mobile  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }
      
    function getReviewInfo($planId)
    {
        //$this->db->select('id, name, email, mobile, roleId');
        $this->db->select('*');
        $this->db->from('mzb_reviews');
        $this->db->where('id', $planId);
        $query = $this->db->get();
        
        return $query->row();
    }
    
    function editReview($planInfo, $planId)
    {
        $this->db->where('id', $planId);
        $this->db->update('mzb_reviews', $planInfo);
        
        return TRUE;
    }
    
    public function get_all_plan_specifications($value='')
    {
        $this->db->select('*');
        $this->db->from('plan_specifications'); 
        $query = $this->db->get();
        return $query->result();
    }
    function deleteReview($planId)
    {
        $this->db->where('id', $planId);
        $this->db->delete('mzb_reviews');
        // die($this->db->last_query())getReviewInfo;
        return $this->db->affected_rows();
    }


}

  