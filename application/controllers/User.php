<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $year_to_fetch = $year ? $year : date('Y');
        $this->global['pageTitle'] = 'Admin : Dashboard';
        $this->global['permision'] = unserialize($_SESSION['permision']);
        $count = $this->user_model->countdashboard();

        $data['userRecords'] = $count;


        $payment_methods = $this->user_model->getPaymentMethods($year_to_fetch);
        $json['status'] = 0;
         
        $residential_rent=0;
        $residential_resale=0;
        $residential_pg=0;
        $commercial_rent=0;
        $commercial_sale=0;
        $residential_flatmates=0;

        $json['year'] = $year_to_fetch;

        if($payment_methods) {
            // print_r($payment_methods); die;
            foreach ($payment_methods as $get) {
                if ($get->property_type == "residential_rent") {
                    $residential_rent++;
                } 
                else if ($get->property_type == "residential_resale") {
                    $residential_resale++;
                }
                else if($get->apartment_type === "residential_pg"){
                    $residential_pg++;
                }
                else if($get->property_type === "commercial_rent"){
                    $commercial_rent++;
                }
                else if($get->property_type === "commercial_sale"){
                    $commercial_sale++;
                }
                else if($get->property_type === "residential_flatmates"){
                    $residential_flatmates++;
                }
            }
        }
        $data['pie_chart'] = array(
          array("label"=> "Residential Rent", "y"=> $residential_rent),
          array("label"=> "Residential Resale", "y"=> $residential_resale),
          array("label"=> "Flatmates", "y"=> $residential_flatmates),
          array("label"=> "PG/Hostel", "y"=> $residential_pg),
          array("label"=> "Commercial Sale", "y"=> $commercial_sale),
          array("label"=> "Commercial Rent", "y"=> $commercial_rent)
        );
        $this->loadViews("dashboard", $this->global, $data , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    // public function earningsGraph($year="", $not_ajax = false) {
    //     //set the year of expenses to show
    //     $year_to_fetch = $year ? $year : date('Y');
        
    //     $earnings = $this->genmod->getYearEarnings($year_to_fetch);
    //     $lastEarnings = 0;
    //     $monthEarnings = array();
    //     $hightEarn['highestEarning'] = 0;
    //     $dataarr = [];
    //     $allMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    //     if ($earnings) {
    //         foreach ($allMonths as $allMonth) {
    //             foreach ($earnings as $get) {
    //                 $earningMonth = date("M", strtotime($get->transDate));
                    
    //                 if ($allMonth == $earningMonth) {
    //                     $lastEarnings += $get->totalPrice;
                        
    //                     $monthEarnings[$allMonth] = $lastEarnings;
    //                 } 
                    
    //                 else {
    //                     if (!array_key_exists($allMonth, $monthEarnings)) {
    //                         $monthEarnings[$allMonth] = 0;
    //                     }
    //                 }
    //             }

    //             if ($lastEarnings > $hightEarn['highestEarning']) {
    //                 $hightEarn['highestEarning'] = $lastEarnings;
    //             }
                
    //             $lastEarnings = 0;
    //         }

    //         foreach ($monthEarnings as $me) {
    //             $dataarr[] = $me;
    //         }
    //     }

    //     else {//if no earning, set earning to 0
    //         foreach ($allMonths as $allMonth) {
    //             $dataarr[] = 0;
    //         }
    //     }

    //     //add info into array
    //     $json = array("total_earnings" => $dataarr, 'earningsYear'=>$year_to_fetch);

    //     //set final output based on where the request is coming from
    //     if($not_ajax){
    //         return $json;
    //     }

    //     else{
    //         $this->output->set_content_type('application/json')->set_output(json_encode($json));
    //     }
    // }
    function paymentMethodChart($year=''){
        ini_set("display_errors", "on");
        $year_to_fetch = $year ? $year : date('Y');
         
        $payment_methods = $this->user_model->getPaymentMethods($year_to_fetch);
        
        $json['status'] = 0;
         
        $residential_rent=0;
        $residential_resale=0;
        $residential_pg=0;
        $commercial_rent=0;
        $commercial_sale=0;
        $residential_flatmates=0;

        $json['year'] = $year_to_fetch;

        if($payment_methods) {
            // print_r($payment_methods); die;
            foreach ($payment_methods as $get) {
                if ($get->property_type == "residential_rent") {
                    $residential_rent++;
                } 
                else if ($get->property_type == "residential_resale") {
                    $residential_resale++;
                }
                else if($get->apartment_type === "residential_pg"){
                    $residential_pg++;
                }
                else if($get->property_type === "commercial_rent"){
                    $commercial_rent++;
                }
                else if($get->property_type === "commercial_sale"){
                    $commercial_sale++;
                }
                else if($get->property_type === "residential_flatmates"){
                    $residential_flatmates++;
                }
            }
            
            //calculate the percentage of each
            $total =$residential_rent+$residential_resale+$residential_pg+$commercial_rent+$commercial_sale
                    +$residential_flatmates;
            
            $residential_rent_percentage = round(($residential_rent/$total) * 100, 2);
            $residential_resale_percentage =  round(($residential_resale/$total) * 100, 2);
            $residential_pg_percentage = round(($residential_pg/$total) * 100, 2);
            $commercial_rent_percentage = round(($commercial_rent/$total) * 100, 2);
            $commercial_sale_percentage = round(($commercial_sale/$total) * 100, 2);
            $residential_flatmates_percentage = round(($residential_flatmates/$total) * 100, 2);
            
            $json['status'] = 1;
            $json['residential_rent'] = $residential_rent_percentage;
            $json['residential_resale'] = $residential_resale_percentage;
            $json['residential_pg'] = $residential_pg_percentage;
            $json['commercial_rent'] = $commercial_rent_percentage;
            $json['commercial_sale'] = $commercial_sale_percentage;
            $json['residential_flatmates'] = $residential_flatmates_percentage;
        }
        
        //set final output
        $this->output->set_content_type('application/json')->set_output(json_encode($json));
    }
    function userListing(){
        if($this->isAdmin() == TRUE){
            $this->loadThis();
        }else{        
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 10 );
           
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Admin : User Listing';
            
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'Admin : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    function settings()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }else{

            if($this->input->post()){

                // echo "<pre>"; 
                // print_r($_FILES);
                $post=$this->input->post('post');
                $post['toplogo']=$_FILES['toplogo']['name'];
                $post['footerlogo']=$_FILES['toplogo']['name'];
                // print_r($post); 

                $result=$this->db->select('*')->from('mzb_settings')->get()->result();
                if(!empty($result)){
                    $this->db->Where('id',1);
                    $this->db->update('mzb_settings',$post);
                }else{

                    $this->db->insert('mzb_settings',$post);
                }
                redirect(base_url('/settings'),'refresh');
                

            }else{

                // $post=$this->input->post('post'); 
                $data['result']=$this->db->select('*')->from('mzb_settings')->get()->row();
                $this->load->model('user_model');
                $data['roles'] = $this->user_model->getUserRoles();
                
                $this->global['pageTitle'] = 'settings';

                $this->loadViews("settings", $this->global, $data, NULL);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            // echo "<pre>";
            // print_r($this->input->post('permissions'));
            // echo serialize($this->input->post('permissions'));
            // die();
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));

                // $address =$this->input->post('address');
                // $dob = $this->input->post('dob');
                // $country =$this->input->post('country');
                // $state = $this->input->post('state');
                // $city = $this->input->post('city');
                // $getwhatsappupdates =$this->input->post('get_whats_app_updates');
                $permissions = serialize($this->input->post('permissions'));

                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'),'permision'=>$permissions);
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'Admin : Edit User';
            
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }
    
    function userDetail($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = 'Admin : Edit User';
            
            $this->loadViews("userDetail", $this->global, $data, NULL);
        }
    }
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $permissions = serialize($this->input->post('permissions'));
               
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name,
                                    'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'),'permision'=>$permissions);
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 
                        'updatedDtm'=>date('Y-m-d H:i:s'),'permision'=>$permissions);
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
     function deleteRole()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $result = $this->user_model->delete_role($userId);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
     function emailUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            
            $userName = $this->input->post('userName');
            $Useremail = $this->input->post('userMail');
            $UserMessage = $this->input->post('userMessage');
            $UserSubject = $this->input->post('userSubject');
            
            //Load email library
            $this->load->library('email');
            
            
            //SMTP & mail configuration
            $config = array(
                'protocol'  => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'dhananjay.webstones@gmail.com',
                'smtp_pass' => 'dhananjay@123',
                'mailtype'  => 'html',
                'charset'   => 'utf-8'
            );
            $this->email->initialize($config);
            $this->email->set_mailtype("html");
            $this->email->set_newline("\r\n");

            //Email content
            $htmlContent = '<h1>Hi '.$userName.'</h1>';
            $htmlContent = 'This is an example mail'.'<br>';
            $htmlContent .= $UserMessage;

            $this->email->to($Useremail);
            $this->email->from('dhananjay.webstones@gmail.com','MyZeroBroker');
            $this->email->subject($UserSubject);
            $this->email->message($htmlContent);

            //Send email
            $this->email->send();
            
            echo(json_encode(array('status'=>TRUE)));
            
//            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
//            
//            $result = $this->user_model->deleteUser($userId, $userInfo);
//            
//            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
//            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Admin : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Admin : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'Admin : My Profile' : 'Admin : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]|callback_emailExists');        
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            
            $userInfo = array('name'=>$name, 'email'=>$email, 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->editUser($userInfo, $this->vendorId);
            
            if($result == true)
            {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }

            redirect('profile/'.$active);
        }
    }
function roleUpdate()
    {
       
            $roleid =$_POST['roleid'];
            $role =$_POST['role'];
            $userInfo = array('role'=>$role);
          
            $result = $this->user_model->update_role($userInfo, $roleid);
            
            if($result == true)
            {
               
                $this->session->set_flashdata('success', 'Role updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Role updation failed');
            }

            redirect('editRole/'.$roleid);
        
    }
    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect('profile/'.$active);
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     * @param {string} $email : This is users email
     */
    function emailExists($email)
    {
        $userId = $this->vendorId;
        $return = false;

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ $return = true; }
        else {
            $this->form_validation->set_message('emailExists', 'The {field} already taken');
            $return = false;
        }

        return $return;
    }

    public function customers($value='')
    {
        $tab = $_GET['en'];
        if($tab === 'etc')
        {
            $en_type = "Extra Services Enquiries";
        }
        if($tab ==='whymzb')
        {
          $en_type = "Why MZB Enquiries";
        }
        $searchText = $this->security->xss_clean($this->input->post('searchText'));
        $data['searchText'] = $searchText;
        
        $this->load->library('pagination');
        
        $count = $this->user_model->customersListCount($searchText,$tab);

        $returns = $this->paginationCompress ( "/customerList/", $count, 10 );
        // print_r($count); print_r($returns); die;
        if($_GET['en']==='etc')
        {
        $data['userRecords'] = $this->user_model->customersList($searchText, $returns["page"], $returns["segment"],$tab);

       }
      if($_GET['en']==='whymzb')
        {
            $data['userRecords'] = $this->user_model->customersList($searchText, $returns["page"], $returns["segment"],$tab);
        }

        
        $this->global['pageTitle'] = 'Admin : User Listing';
        $this->global['enType'] = $en_type;
        // echo "<pre>"; print_r($data); die;
        $this->loadViews("users/customers", $this->global, $data, NULL);
    }

    public function view_service_data($value='')
    {

         $services = $_GET['type'];
          $tab = $_GET['en'];
        $this->load->library('pagination');
        
        $count = $this->user_model->serviceCustomersListCount($searchText,$tab);

        $returns = $this->paginationCompress ( "/customerList/", $count, 10 );
        // print_r($count); print_r($returns); die;
       if($tab === 'extra_services')
        {
            $en_type = "Extra Services Enquiries";
        }
        if($tab ==='why_myzerobroker')
        {
          $en_type = "Why MZB Enquiries";
        }
        $data['userRecords'] = $this->user_model->serviceCustomersList($searchText, $returns["page"], $returns["segment"],$services);
        $this->global['pageTitle'] = 'Admin : User Listing';
        $this->global['enType'] = $en_type;
        $this->loadViews("users/view_service", $this->global, $data, NULL);
     
    }
public function role()
    {
        
       
            $data['roles'] = $this->user_model->getRoles();
            
            $this->global['pageTitle'] = 'Admin : Add New User';

            $this->loadViews("addRole", $this->global, $data, NULL);
        // $this->loadViews("addRole", $this->global, NULL , NULL);
    }
    public function editRole($id)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
               
            
            $data = $this->user_model->getEditRoles($id);
            $this->global['rolename'] = $data[0]->role;
            $this->global['roleId'] = $data[0]->id;
           
            $this->global['pageTitle'] = 'Admin : Edit User';
            
            $this->loadViews("edit_role", $this->global, $data, NULL);
        }
    }

    public function addNewRole()
    {

        $this->global['pageTitle'] = 'Admin : Dashboard';
        $this->global['permision'] = unserialize($_SESSION['permision']);
        $this->loadViews("addNewRole", $this->global, NULL , NULL);
        
      
    }
    public function addRole()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            // echo "<pre>";
            // print_r($this->input->post('role'));
          
            // die();
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('role','Role','trim|required|max_length[128]');
            
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $role = ucwords(strtolower($this->security->xss_clean($this->input->post('role'))));
               

                $userInfo = array('role'=>$role);
                
                $this->load->model('user_model');
                $result = $this->user_model->addnew_role($userInfo);


                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New role created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Role creation failed');
                }
                
                redirect('addNewRole');
            }
        }
    }

}

?>
