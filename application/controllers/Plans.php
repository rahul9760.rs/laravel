<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Plans extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('plan_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->plan_model->planListingCount($searchText);

			$returns = $this->paginationCompress ( "plans/", $count, 10 );
           
            $data['userRecords'] = $this->plan_model->planListing($searchText, $returns["page"], $returns["segment"]);
           
            $this->global['pageTitle'] = 'Admin : User Listing';
        
        $this->loadViews("plans/plans", $this->global,  $data , NULL);
    }

    function plan_type()
    {
     
         $this->global['plan_type'] = $_GET['plan_type'];
         $this->loadViews("plans/view_plan", $this->global, NULL , NULL);
        // die();
    }
 function plan_name()
 {
      $plan_name = $this->global['plan_name'] = $_GET['plan_name'];
      $plan_for = $this->global['plan_for'] = $_GET['plan_for'];

       $data['planInfo'] = $this->plan_model->getPlanInfo($plan_name,$plan_for);
      
       $data['specification_list'] = $this->plan_model->get_all_plan_specifications($plan_name,$plan_for);
       $this->global['pageTitle'] = 'Admin : Edit Plan';
       $this->loadViews("plans/addNewPlan", $this->global, $data , NULL);
 }
 function addPlan()
 {
    // $plan_name = $this->security->xss_clean($this->input->post('plan_name'));
    // $plan_price = $this->security->xss_clean($this->input->post('plan_price'));
    // $spec = serialize($this->input->post('spec'));
    // $plan_for = $this->security->xss_clean($this->input->post('plan_for'));
    // $validity = $this->security->xss_clean($this->input->post('validity'));
    // $planInfo = array('plan_name'=>$plan_name, 'plan_for'=>$plan_for,'price'=>$plan_price,'validity'=>$validity, 'plan_specification'=>$spec);
                
    // $result = $this->plan_model->addPlan($planInfo);
    $planId = $this->security->xss_clean($this->input->post('planId'));
    $plan_name = $this->security->xss_clean($this->input->post('plan_name'));
    $plan_price = $this->security->xss_clean($this->input->post('plan_price'));
    $plan_for = $this->security->xss_clean($this->input->post('plan_for'));


    $validity = $this->security->xss_clean($this->input->post('validity'));

    $web_only = $this->security->xss_clean($this->input->post('web_only'));
    $privacy = $this->security->xss_clean($this->input->post('privacy'));
    $filertation = $this->security->xss_clean($this->input->post('filertation'));
    $alert = $this->security->xss_clean($this->input->post('alert'));
    $legal_assistance = $this->security->xss_clean($this->input->post('legal_assistance'));
    $home_loan = $this->security->xss_clean($this->input->post('home_loan'));
    $promotion = $this->security->xss_clean($this->input->post('promotion'));
    $relationship = $this->security->xss_clean($this->input->post('relationship'));
    $photoshoot = $this->security->xss_clean($this->input->post('photoshoot'));
    $site_assistance = $this->security->xss_clean($this->input->post('site_assistance'));
    $arr = array(
            'web_only' => ($web_only!='')?$web_only:"n/a", 
            'privacy' => ($web_only!='')?$privacy:"n/a", 
            'filertation' => ($web_only!='')?$filertation:"n/a", 
            'alert' => ($web_only!='')?$alert:"n/a", 
            'legal_assistance' => ($web_only!='')?$legal_assistance:"n/a", 
            'home_loan' => ($web_only!='')?$home_loan:"n/a", 
            'promotion' => ($web_only!='')?$promotion:"n/a", 
            'photoshoot' => ($web_only!='')?$photoshoot:"n/a", 
            'site_assistance' => ($web_only!='')?$site_assistance:"n/a", 
            'relationship' => ($web_only!='')?$relationship:"n/a", 
        );
    $spec = serialize($arr);
    // echo "<pre>"; print_r($arr); die;
    $planInfo = array('plan_name'=>$plan_name, 'plan_for'=>$plan_for,'price'=>$plan_price,'validity'=>$validity, 'plan_specification'=>$spec);
                
                
          
                
                $result = $this->plan_model->editPlan($planInfo, $planId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Plan updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Plan updation failed');
                }
  

    redirect(base_url("plan?plan_for=$plan_name&plan_name=$plan_for"));
    //$this->loadViews("plans/view_plan", $this->global, $data, NULL);
 }
    /**
     * This function is used to load the add new form
     */
    function addNewPlan()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $post=$this->input->post();
            // echo "<pre>"; print_r($post); die;
            if($this->input->post()){
                $arr = array(
                
                    'plan_name' => $this->input->post('plan_name'),
                    'price' => $this->input->post('plan_price') ,
                    'validity' => $this->input->post('validity') ,
                    'plan_specification' => serialize($this->input->post('spec')),
                );
                
                if($this->plan_model->add('mzb_plans',$arr)){
                    $this->session->set_flashdata('message', 'Plan Added Successfully');
                    $this->session->set_flashdata('type', 'success');
                    redirect(base_url('plans/addNewPlan'));
                }else{
                     $this->session->set_flashdata('message', 'Something Went Wrong');
                    $this->session->set_flashdata('type', 'danger');
                    redirect(base_url('plans/addNewPlan'));
                }
            }else{
                $this->load->model('user_model');
                $data['roles'] = $this->user_model->getUserRoles();
                $data['specification_list'] = $this->plan_model->get_all_plan_specifications();

                $this->global['pageTitle'] = 'Admin : Add New User';

                $this->loadViews("plans/addNewPlan", $this->global, $data, NULL);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));

                $address =$this->input->post('address');
                $dob = $this->input->post('dob');
                $country =$this->input->post('country');
                $state = $this->input->post('state');
                $city = $this->input->post('city');
                $getwhatsappupdates =$this->input->post('get_whats_app_updates');

                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'),'address'=>$address,'dob'=>$dob,'country'=>$country,'address'=>$address,'state'=>$state,'city'=>$city,'get_whats_app_updates'=>$getwhatsappupdates);
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($planId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
//            die($userId);
            if($planId == null)
            {
                redirect('planListing');
            }
            
//            $data['roles'] = $this->user_model->getUserRoles();
            $data['planInfo'] = $this->plan_model->getPlanInfo($planId);
            $data['specification_list'] = $this->plan_model->get_all_plan_specifications();
            $this->global['pageTitle'] = 'Admin : Edit Plan';
            
            $this->loadViews("plans/editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editPlan()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $planId = $this->input->post('planId');
            
            $this->form_validation->set_rules('plan_name','Plan Name','trim|required');
            $this->form_validation->set_rules('price','Price','trim|required');
            $this->form_validation->set_rules('validity','Validity','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($planId);
            }
            else
            {
                $plan_name = ucwords(strtolower($this->security->xss_clean($this->input->post('plan_name'))));
                $plan_price = strtolower($this->security->xss_clean($this->input->post('price')));
                $validity = $this->input->post('validity');
                $specification = $this->input->post('spec');
                
                $planInfo = array();
                
                
                    $planInfo = array(
                        'plan_name' => $plan_name,
                        'price' => $plan_price,
                        'validity' => $validity,
                        'plan_specification' => serialize($specification),
                        );
                
                
                $result = $this->plan_model->editPlan($planInfo, $planId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Plan updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Plan updation failed');
                }
                
                redirect("plans/editOld/$planId");
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deletePlan()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $planId = $this->input->post('planId');
//            $planInfo = array('updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->plan_model->deletePlan($planId);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Admin : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'Admin : User Login History';
            
            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'Admin : My Profile' : 'Admin : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]|callback_emailExists');        
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            
            $userInfo = array('name'=>$name, 'email'=>$email, 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->editUser($userInfo, $this->vendorId);
            
            if($result == true)
            {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }

            redirect('profile/'.$active);
        }
    }

    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect('profile/'.$active);
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     * @param {string} $email : This is users email
     */
    function emailExists($email)
    {
        $userId = $this->vendorId;
        $return = false;

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ $return = true; }
        else {
            $this->form_validation->set_message('emailExists', 'The {field} already taken');
            $return = false;
        }

        return $return;
    }
}

?>