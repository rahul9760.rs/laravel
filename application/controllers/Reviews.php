<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Reviews extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();

        error_reporting(-1);
        ini_set('display_errors', 'On');
        $this->load->model('user_model');
        $this->load->model('plan_model');
        $this->load->model('reviews_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->reviews_model->reviewsListingCount($searchText);

			$returns = $this->paginationCompress("reviews/", $count, 10 );
            
            $data['userRecords'] = $this->reviews_model->reviewsListing($searchText, $returns["page"], $returns["segment"]);
            // echo "<pre>"; print_r($data['userRecords']); die;
            $this->global['pageTitle'] = 'Admin : User Listing';
        
        $this->loadViews("reviews/reviews", $this->global,  $data , NULL);
    }

    /**
     * This function is used to load the add new form
     */
    function addNewReview()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $post=$this->input->post();
            // echo "<pre>"; print_r($post); die;
            if($this->input->post()){
                $arr = array(
                
                    'review' => $this->input->post('review'),
                    'review_by' => $this->input->post('review_by') ,
                    'designation' => $this->input->post('designation'),
                    'rating' => $this->input->post('rating'),
                );
                
                if($this->reviews_model->add('mzb_reviews',$arr)){
                    $this->session->set_flashdata('message', 'Reviews Added Successfully');
                    $this->session->set_flashdata('type', 'success');
                    redirect(base_url('reviews/addNewReview'));
                }else{
                     $this->session->set_flashdata('message', 'Something Went Wrong');
                    $this->session->set_flashdata('type', 'danger');
                    redirect(base_url('reviews/addNewReview'));
                }
            }else{
                $this->load->model('user_model');
                $data['roles'] = $this->user_model->getUserRoles();
                $data['specification_list'] = $this->reviews_model->get_all_plan_specifications();

                $this->global['pageTitle'] = 'Admin : Add New User';

                $this->loadViews("reviews/addNewReview", $this->global, $data, NULL);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
     

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($planId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }

        else
        {
//            die($userId);
            if($planId == null)
            {
                redirect('planListing');
            }
            
//            $data['roles'] = $this->user_model->getUserRoles();
            $data['reviewInfo'] = $this->reviews_model->getReviewInfo($planId);
            $data['specification_list'] = $this->reviews_model->get_all_plan_specifications();
            $this->global['pageTitle'] = 'Admin : Edit Plan';
            
            $this->loadViews("reviews/editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editReview()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $planId = $this->input->post('planId');
            
            $this->form_validation->set_rules('review','review','trim|required');
            $this->form_validation->set_rules('review_by','review_by','trim|required');
            $this->form_validation->set_rules('designation','designation','trim|required');
            $this->form_validation->set_rules('rating','rating','trim|required');
             
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($planId);
            }
            else
            {
                $review = ucwords(strtolower($this->security->xss_clean($this->input->post('review'))));
                $review_by = strtolower($this->security->xss_clean($this->input->post('review_by')));
                $designation = $this->input->post('designation');
                $rating = $this->input->post('rating');
                
                $planInfo = array();
                
                
                    $planInfo = array(
                        'review' => $review,
                        'review_by' => $review_by,
                        'designation' => $designation,
                        'rating' => $rating,
                        );
                
                
                $result = $this->reviews_model->editReview($planInfo, $planId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Review updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Review updation failed');
                }
                
                redirect("reviews/editOld/$planId");
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteReview()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $planId = $this->input->post('userId');
//            $planInfo = array('updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->reviews_model->deleteReview($planId);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Admin : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
 
 
}

?>