/**
 * @author Kishor Mali
 */


jQuery(document).ready(function () {

    jQuery(document).on("click", ".emailUser", function () {

        $('#emailModal').modal('show');

    });

    jQuery(document).on("click", "#send_mail", function () {
        var hitURL = baseURL + "emailUser";
        var userMail = $('.emailUser').data("mail");
        var userName = $('.emailUser').data("name");
        var userSubject = $('#email_subject').val();
        var userMessage = $('#email_message').val();
        var confirmation = confirm("Are you sure to email this user ?");

        if (confirmation) {
            $(this).html('Sending...');
            $(this).attr('disabled', 'true');
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    userName: userName,
                    userMail: userMail,
                    userSubject: userSubject,
                    userMessage: userMessage
                }
            }).done(function (data) {
                if (data.status = true) {
                    console.log(data);
                    $('#send_mail').removeAttr('disabled');                    
                    $('#send_mail').html('Send');
                    $('#emailModal').modal('hide');
                    alert("Email Has Been Sent");
                } else {
                    alert("Email Sending Failed");
                }
            });
        }
    });

    jQuery(document).on("click", ".deleteUser", function () {
        var userId = $(this).data("userid"),
            hitURL = baseURL + "deleteUser",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this user ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    userId: userId
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("User successfully deleted");
                } else if (data.status = false) {
                    alert("User deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });

    jQuery(document).on("click", ".deleteReview", function () {
        var userId = $(this).data("planid"),
            hitURL = baseURL + "reviews/deleteReview",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this Review ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    userId: userId
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("User successfully deleted");
                } else if (data.status = false) {
                    alert("User deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });
    //deleteProperty
    jQuery(document).on("click", ".deleteProperty", function () {
        var msg = $(this).data("msg");
        var url = $(this).data("url");
        var propertyid = $(this).data("propertyid"),
            hitURL = baseURL + url,
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this Property ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    propertyid: propertyid
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert(msg + " successfully deleted");
                } else if (data.status = false) {
                    alert("Property deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });
    jQuery(document).on("click", ".makeActive", function () {
        var msg = $(this).data("msg");
        var url = $(this).data("url");
        var propertyid = $(this).data("propertyid"),
            hitURL = baseURL + url,
            currentRow = $(this);

        var confirmation = confirm("Are you sure want to make active this Property ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    propertyid: propertyid
                }
            }).done(function (data) {
                // console.log(data);
                // currentRow.parents('tr').remove();
                if (status = "true") {
                    alert(" successfully active");
                } else if (status = "false") {
                    alert("Property activation failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });
    //deleteResidentialResaleProperty
    // jQuery(document).on("click", ".deleteResidentialResaleProperty", function(){
    // 	var propertyid = $(this).data("propertyid"),
    // 		hitURL = baseURL + "deleteResidentialResaleProperty",
    // 		currentRow = $(this);

    // 	var confirmation = confirm("Are you sure to delete this Property ?");

    // 	if(confirmation)
    // 	{
    // 		jQuery.ajax({
    // 		type : "POST",
    // 		dataType : "json",
    // 		url : hitURL,
    // 		data : { propertyid : propertyid } 
    // 		}).done(function(data){
    // 			console.log(data);
    // 			currentRow.parents('tr').remove();
    // 			if(data.status = true) { alert("Property successfully deleted"); }
    // 			else if(data.status = false) { alert("Property deletion failed"); }
    // 			else { alert("Access denied..!"); }
    // 		});
    // 	}
    // });

    jQuery(document).on("click", ".searchList", function () {

    });
});

// function deletefn(moduleid,url,module='this'){
// 	var propertyid = moduleid,
// 		hitURL = baseURL + url,
// 		currentRow = $(this);

// 	var confirmation = confirm("Are you sure to delete this Property ?");

// 	if(confirmation)
// 	{
// 		jQuery.ajax({
// 		type : "POST",
// 		dataType : "json",
// 		url : hitURL,
// 		data : { propertyid : propertyid } 
// 		}).done(function(data){
// 			console.log(data);
// 			currentRow.parents('tr').remove();
// 			if(data.status = true) { alert("Property successfully deleted"); }
// 			else if(data.status = false) { alert("Property deletion failed"); }
// 			else { alert("Access denied..!"); }
// 		});
// 	}
// }
