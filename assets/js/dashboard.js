'use strict';
var appRoot="http://"+window.location.host+"/";
$(document).ready(function() {
    //checkDocumentVisibility(checkLogin);//check document visibility in order to confirm user's log in status
    var appRoot="http://"+window.location.host;
    //get earnings for current  year on page load
    getEarnings();
    getPropertiesPieChart();
    //load payment method pie charts
    loadPaymentMethodChart();
    
    
    //WHEN "YEAR" IS CHANGED IN ORDER TO CHANGE THE YEAR OF ACCOUNT BEING SHOWN
    $("#earningAndExpenseYear").change(function(){
        var year = $(this).val();
        
        if(year){
            $("#yearAccountLoading").html("<i class='"+spinnerClass+"'></i> Loading...");
            
            //get earnings for current  year on page load
            getEarnings(year);
            
            //also get the payment menthods for that year
            loadPaymentMethodChart(year);
        }
    });
});


/*
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
*/
/*
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
*/

/**
 * 
 * @param {type} year
 * @returns {undefined}
 */
function getEarnings(year){
    var yearToFetch = year || '';
    
    $.ajax({
        type: 'GET',
        url: appRoot+"dashboard/earningsGraph/"+yearToFetch,
        dataType: "html"
    }).done(function(data){
        var response = jQuery.parseJSON(data);

        var barChartData = {
          labels : ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"],
          datasets : [{
            fillColor : "rgba(255,255,255,1)", // bar color
            strokeColor : "rgba(151,187,205,0.8)", //hover color
            highlightFill : "rgba(242,245,233,1)", // highlight color
            highlightStroke : "rgba(151,187,205,1)", // highlight hover color
            data : response.total_earnings
          }]
        };

        //show the expense title
        document.getElementById('earningsTitle').innerHTML = "Earnings (" + response.earningsYear +")";

        var earningsGraph = document.getElementById("earningsGraph").getContext("2d");

        window.myBar = new Chart(earningsGraph).Bar(barChartData, {
          responsive : true,
          scaleGridLineColor : "rgba(255,255,255,1)",
          scaleShowHorizontalLines: true,
          scaleShowVerticalLines: false,
          barStrokeWidth : 1,
          barValueSpacing : 20
        });

        //remove the loading info
        $("#yearAccountLoading").html("");
    }).fail(function(){
        console.log('req failed');
    });
}

/*
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
********************************************************************************************************************************
*/


/**
 * 
 * @returns {undefined}
 */

 function getPropertiesPieChart(argument) {
     // body...
 }
function loadPaymentMethodChart(year){
    var yearToGet = year ? year : "";
    
    $.ajax({
        type: 'GET',
        url: appRoot+"user/paymentmethodchart/"+yearToGet,
        dataType: "html",
        success: function(data) {
            var response = jQuery.parseJSON(data);
            var residential_rent = response.residential_rent;
            var residential_resale = response.residential_resale;
            var residential_pg = response.residential_pg;
            var commercial_rent = response.commercial_rent;
            var commercial_sale = response.commercial_sale;
            var residential_flatmates = response.residential_flatmates;

            if(response.status === 1) {
                if((residential_rent === 0 && residential_resale === 0 && residential_pg === 0 && commercial_rent === 0 && commercial_sale === 0 && residential_flatmates === 0)) {
                  var paymentMethodData = [{
                    value: 1,
                    color:"#4D5360",
                    highlight: "#616774",
                    label: "No Payment"
                  }];
                } 

                else {
                  var paymentMethodData = [{
                    value: residential_rent,
                    color:"#084D5F",
                    highlight: "#009587",
                    label: "Residential Rent"
                  }, {
                    value: residential_resale,
                    color: "#009587",
                    highlight: "#009587",
                    label: "Residential Resale"
                  }, {
                    value: residential_pg,
                    color: "#333",
                    highlight: "pink",
                    label: "PG/Hostel"
                  }, {
                    value: commercial_rent,
                    color: "#333",
                    highlight: "pink",
                    label: "Commercial Rent"
                  }, {
                    value: commercial_sale,
                    color: "#333",
                    highlight: "pink",
                    label: "Commercial Sale"
                  }, {
                    value: residential_flatmates,
                    color: "#333",
                    highlight: "pink",
                    label: "Flatmates"
                  }
                  ];
                }
            } 

            else {//if status is 0
                var paymentMethodData = [{
                  value: 1,
                  color:"#4D5360",
                  highlight: "#616774",
                  label: "No Payment"
                }];
            }
          
            var ctx = document.getElementById("paymentMethodChart").getContext("2d");
            window.myPie = new Chart(ctx).Pie(paymentMethodData);

            //remove the loading info
            $("#yearAccountLoading").html("");
            
            //append the year we are showing
            $("#paymentMethodYear").html(" - "+response.year);
        }
    });
}