/**
 * @author Kishor Mali
 */

//deleteUser
jQuery(document).ready(function () {

    jQuery(document).on("click", ".deleteUser", function () {
        var userId = $(this).data("userid"),
            hitURL = baseURL + "deleteUser",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this user ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    userId: userId
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("User successfully deleted");
                } else if (data.status = false) {
                    alert("User deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });
    //deletePlan
    jQuery(document).on("click", ".deletePlan", function () {
        var planId = $(this).data("planid"),
            hitURL = baseURL + "plans/deletePlan",
            currentRow = $(this);
        //		  alert(hitURL);
        var confirmation = confirm("Are you sure to delete this plan ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    planId: planId
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("Plan successfully deleted");
                } else if (data.status = false) {
                    alert("Plan deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });
    //deletePayment
    jQuery(document).on("click", ".deletePayment", function () {
        var paymentId = $(this).data("paymentid"),
            hitURL = baseURL + "payments/deletePayment",
            currentRow = $(this);
        //		  alert(hitURL);
        var confirmation = confirm("Are you sure to delete this Payment ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    paymentId: paymentId
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("Payment successfully deleted");
                } else if (data.status = false) {
                    alert("Payment deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });
    //deleteProperty
    jQuery(document).on("click", ".deleteProperty", function () {
        var propertyid = $(this).data("propertyid"),
            hitURL = baseURL + "deleteResidentialRentProperty",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this Property ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    propertyid: propertyid
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("Property successfully deleted");
                } else if (data.status = false) {
                    alert("Property deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });

    //deleteResidentialResaleProperty
    jQuery(document).on("click", ".deleteResidentialResaleProperty", function () {
        var propertyid = $(this).data("propertyid"),
            hitURL = baseURL + "deleteResidentialResaleProperty",
            currentRow = $(this);

        var confirmation = confirm("Are you sure to delete this Property ?");

        if (confirmation) {
            jQuery.ajax({
                type: "POST",
                dataType: "json",
                url: hitURL,
                data: {
                    propertyid: propertyid
                }
            }).done(function (data) {
                console.log(data);
                currentRow.parents('tr').remove();
                if (data.status = true) {
                    alert("Property successfully deleted");
                } else if (data.status = false) {
                    alert("Property deletion failed");
                } else {
                    alert("Access denied..!");
                }
            });
        }
    });

    jQuery(document).on("click", ".searchList", function () {

    });
});

function deletefn(moduleid, url, module = 'this') {
    var propertyid = moduleid,
        hitURL = baseURL + url,
        currentRow = $(this);

    var confirmation = confirm("Are you sure to delete this Property ?");

    if (confirmation) {
        jQuery.ajax({
            type: "POST",
            dataType: "json",
            url: hitURL,
            data: {
                propertyid: propertyid
            }
        }).done(function (data) {
            console.log(data);
            currentRow.parents('tr').remove();
            if (data.status = true) {
                alert("Property successfully deleted");
            } else if (data.status = false) {
                alert("Property deletion failed");
            } else {
                alert("Access denied..!");
            }
        });
    }
}
